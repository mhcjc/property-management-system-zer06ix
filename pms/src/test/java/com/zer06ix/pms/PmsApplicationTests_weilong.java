package com.zer06ix.pms;

import com.zer06ix.pms.controller.EmailTestController;
import com.zer06ix.pms.controller.RepairController;
import com.zer06ix.pms.controller.UserController;
import com.zer06ix.pms.controller.WepayController;
import com.zer06ix.pms.entity.*;
import com.zer06ix.pms.mapper.*;
import com.zer06ix.pms.service.MailService;
import com.zer06ix.pms.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.List;
import java.util.Map;

@SpringBootTest
class PmsApplicationTests_weilong {

    /*@Test
    void contextLoads() {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(PmsApplication.class);
//        UserServiceImpl userService = ctx.getBean("userServiceImpl", UserServiceImpl.class);
        UserMapper userMapper = ctx.getBean("userMapper", UserMapper.class);
        String 张三 = "张三";
        userMapper.queryUserByName(张三);
        System.out.println("张三 = " + 张三);

    }*/
    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private UserService userService;

    @Autowired
    private UserController userController;

    @Autowired
    private WepayController wepayController;

    @Autowired
    private EmailTestController emailTestController;

    @Autowired
    private MailService mailService;

    @Autowired
    private BedMapper bedMapper;

    @Autowired
    private  UserMapper userMapper;

    @Autowired
    private BuildMapper buildMapper;

    @Autowired
    private ProjectMapper projectMapper;

    @Autowired
    private RoomMapper roomMapper;

    @Autowired
    private SuiteMapper suiteMapper;

    @Autowired
    private  WepayMapper wepayMapper;

    @Autowired
    private RepairController repairController;

    @Test
    void contextLoads() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("1161530087@qq.com");
        message.setTo("1354253128@qq.com");
        message.setSubject("测试");
        message.setText("验证码:"+"111");
        javaMailSender.send(message);
    }

    /**
     * 用于测试:用户注册
     */
    @Test
    public void test01() {
    }

    /**
     * 用于测试:
     */
    @Test
    public void test02() {
        List<Bed> beds = bedMapper.queryBedByRoomId(10002);
        System.out.println("beds = " + beds);

        List<Build> builds = buildMapper.queryBuildByProjectId(10001);
        System.out.println("builds = " + builds);

        List<Project> Projects = projectMapper.queryAllProjects();
        System.out.println("Projects = " + Projects);

        List<Room> rooms = roomMapper.queryRoomBySuiteId(10004);
        System.out.println("rooms = " + rooms);

        List<Suite> suites = suiteMapper.querySuiteByBuildId(10006);
        System.out.println("suites = " + suites);
    }

    /**
     * 用于测试:
     */
    @Test
    public void test03() {
        Integer flag = wepayMapper.updateWepaymentByRoomId(10001,"微信");
        System.out.println("flag = " + flag);
    }

    /**
     * 用于测试:
     */
    @Test
    public void test04() {
       /* List<Wepayment> wepayment = wepayMapper.queryWepaymentByRoomId(10001);
        System.out.println("wepayment = " + wepayment);*/
    }

    /**
     * 用于测试:
     */
    @Test
    public void test05() {
        /*System.out.println(wepayController.personalExpensesInfo("10002"));
        System.out.println(wepayController.paySuccess("10002","微信"));*/
    }

    /**
     * 用于测试:
     */
    @Test
    public void test06() {
        System.out.println(userController.login("10001","222222"));
    }

    /**
     * 用于测试:
     */
    @Test
    public void test07() {
        List<RoomRepair> roomRepairs = roomMapper.queryRoomRepairById(10001);
        Map<String, Object> stringObjectMap = repairController.repairInfo("10001");
        System.out.println("roomRepairs = " + roomRepairs);
        System.out.println("stringObjectMap = " + stringObjectMap);
    }

    /**
     * 用于测试:email
     */
    @Test
    public void test08() {
        User user = userMapper.queryUserByEmailAndId(10001,"1354253128@qq.com");
        System.out.println("user = " + user);
    }

    /**
     * 用于测试:email
     */
    @Test
    public void test09() {
        User user = userMapper.queryUserById(10001);
        System.out.println("user = " + user);
    }
}
