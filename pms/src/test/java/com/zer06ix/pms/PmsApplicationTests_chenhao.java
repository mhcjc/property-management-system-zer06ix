package com.zer06ix.pms;


import com.zer06ix.pms.controller.CheckOutController;
import com.zer06ix.pms.controller.GoodsInspectionController;
import com.zer06ix.pms.entity.CheckOut;
import com.zer06ix.pms.entity.GoodsInspection;
import com.zer06ix.pms.entity.Room;
import com.zer06ix.pms.mapper.CheckOutMapper;
import com.zer06ix.pms.mapper.GoodInspectionMapper;
import com.zer06ix.pms.mapper.RoomMapper;
import com.zer06ix.pms.service.CheckOutService;
import com.zer06ix.pms.service.GoodsInsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class PmsApplicationTests_chenhao {

    @Autowired
    private RoomMapper roomMapper;

    @Autowired
    private GoodInspectionMapper goodInspectionMapper;

    @Autowired
    private GoodsInsService goodsInsService;

    @Autowired
    private GoodsInspectionController goodsInspectionController;

    @Autowired
    private CheckOutMapper checkOutMapper;

    @Autowired
    private CheckOutService checkOutService;

    @Autowired
    private CheckOutController checkOutController;

    @Test
    public void test01() {
        List<Room> rooms = roomMapper.queryRoomExist();
        for (Room room : rooms) {
            System.out.println(room);
        }
    }

    @Test
    public void test02(){
        BigDecimal a = new BigDecimal("20.4");
        GoodsInspection goodsInspection = new GoodsInspection();
        goodsInspection.setCheckOutId(10002);
        goodsInspection.setGoodsName("水杯");
        goodsInspection.setGoodsPrice(a);
        goodsInspection.setGoodsStatus(0);
        goodsInspection.setRemark("失手打碎");

        Integer inspectionId = goodInspectionMapper.addGoodInspection(goodsInspection);
        System.out.println("生成的物品损坏表的id为：" + inspectionId);
    }

    @Test
    public void test03(){
        Room room = goodInspectionMapper.queryRoomByCheckoutId(10002);
        System.out.println("room：" + room);
    }

    @Test
    public void test04(){
        List<GoodsInspection> goodsInspections = goodInspectionMapper.queryAllGoodsInspection();
        for (GoodsInspection goodsInspection : goodsInspections) {
            System.out.println(goodsInspection);
        }
    }

    @Test
    public void test05(){
        GoodsInspection goodsInspection = goodInspectionMapper.queryGoodsInsByinspectionId(10002);
        System.out.println(goodsInspection);
    }

    @Test
    public void test06(){
        List<GoodsInspection> goodsInspections = new ArrayList<GoodsInspection>();
        GoodsInspection goodsInspection = new GoodsInspection();
        BigDecimal p = new BigDecimal("5");
        for (int i = 1 ; i < 4 ; i++){
            goodsInspection.setCheckOutId(10003);
            goodsInspection.setGoodsName("衣裤架");
            goodsInspection.setGoodsPrice(p);
            goodsInspection.setGoodsStatus(0);
            goodsInspections.add(goodsInspection);
        }
        System.out.println(goodsInspections);
        /*Map<String, Object> map = goodsInspectionController.addGoodInspection(goodsInspections);
        System.out.println("map" + map);*/
    }

    @Test
    public void test07(){
//        Map<String, Object> map = goodsInspectionController.updateGoodsInsStatus("10004");
//        System.out.println(map);
    }

    @Test
    public void test08(){
        Map<String, Object> map = goodsInspectionController.queryRoomByCheckoutId("10003");
        System.out.println("map" + map);
    }

    @Test
    public void test09(){
        Map<String, Object> map = goodsInspectionController.queryGoodsInsBycheckOutId("10002");
        System.out.println(map);
    }

    @Test
    public void test10(){
        Date resideDate = checkOutMapper.queryResideDate(10002);
        Date resideDate2 = checkOutMapper.queryResideDate(10003);
        System.out.println((resideDate2.getTime() - resideDate.getTime()) / (24 * 60 * 60 * 1000) + "天");
    }

    @Test
    public void test11(){
        CheckOut checkOut = checkOutMapper.queryCheckOutByUserId(10001);
        checkOut.setUsername("李四");
        checkOut.setUserId(10002);
        checkOut.setRoomId(10003);
        checkOut.setRoomAddress("温馨99345室");
        Integer i = checkOutService.insertCheckOut(checkOut);
        System.out.println("i" +i);
    }

    @Test
    public void test12(){
        BigDecimal account = goodsInsService.account(10001);
        System.out.println("account" + account);
    }
}



