package com.zer06ix.pms;

import com.zer06ix.pms.controller.HouseController;
import com.zer06ix.pms.controller.UserController;
import com.zer06ix.pms.entity.*;
import com.zer06ix.pms.mapper.*;
import com.zer06ix.pms.service.CheckInService;
import com.zer06ix.pms.service.UserService;
import com.zer06ix.pms.service.serviceImpl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.List;
import java.util.Map;

@SpringBootTest
class PmsApplicationTests_jiacong {

    /*@Test
    void contextLoads() {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(PmsApplication.class);
//        UserServiceImpl userService = ctx.getBean("userServiceImpl", UserServiceImpl.class);
        UserMapper userMapper = ctx.getBean("userMapper", UserMapper.class);
        String 张三 = "张三";
        userMapper.queryUserByName(张三);
        System.out.println("张三 = " + 张三);

    }*/
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private UserService userService;

    @Autowired
    private UserController userController;

    @Autowired
    private BedMapper bedMapper;

    @Autowired
    private BuildMapper buildMapper;

    @Autowired
    private ProjectMapper projectMapper;

    @Autowired
    private RoomMapper roomMapper;

    @Autowired
    private SuiteMapper suiteMapper;

    @Autowired
    private HouseController houseController;

    @Autowired
    private CheckInMapper checkInMapper;

    @Autowired
    private CheckInService checkInService;

    @Test
    void contextLoads() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("1161530087@qq.com");
        message.setTo("1354253128@qq.com");
        message.setSubject("测试");
        message.setText("验证码:"+"111");
        javaMailSender.send(message);
    }

    /**
     * 用于测试:用户注册
     */
    @Test
    public void test01() {
        User user = new User();
        user.setPhoneNumber("13744561123");
    }

    /**
     * 用于测试:
     */
    @Test
    public void test02() {
        /*List<Bed> beds = bedMapper.queryBedByRoomId(10002);
        System.out.println("beds = " + beds);

        List<Build> builds = buildMapper.queryBuildByProjectId(10001);
        System.out.println("builds = " + builds);

        List<Project> Projects = projectMapper.queryAllProjects();
        System.out.println("Projects = " + Projects);

        List<Room> rooms = roomMapper.queryRoomBySuiteId(10004);
        System.out.println("rooms = " + rooms);

        List<Suite> suites = suiteMapper.querySuiteByBuildId(10006);
        System.out.println("suites = " + suites);*/


        Room room = roomMapper.queryRoomById(10001);
        room.setBedLast(room.getBedLast()-1);
        Integer result = roomMapper.updateRoom(room);
        System.out.println("result = " + result);
    }

    /**
     * 用于测试:{@link HouseController}
     */
    @Test
    public void test03() {
    }
    
    /**
     * 用于测试:结算查验ChechInMapper
     */
    @Test
    public void test04() {
        List<CheckIn> checkIns = checkInMapper.queryCheckInByUserId(10007);
        System.out.println(checkIns);
        for (CheckIn checkIn : checkIns) {
            Integer integer = checkInMapper.addCheckIn(checkIn);
            System.out.println(integer+"+++++++++++++++");
        }
     }


     /**
      * 用于测试:CheckInService
      */
     @Test
     public void test05() {
         User user = userMapper.queryUserById(10006);
         System.out.println("user = " + user);
         List<CheckIn> checkIns = checkInService.queryCheckIn(user);
         System.out.println("checkIns = " + checkIns);
        /* for (CheckIn checkIn : checkIns) {
             checkInService.approveCheckIn(checkIn);
         }*/
     }
     
     /**
      * 用于测试:UserMapper
      */
     @Test
     public void test06() {
         User user1 = new User();
         user1.setUsername("10022");
         user1.setPassword("222222");
         user1.setEmail("user.getEmail()");
         user1.setPhoneNumber("user.get");
         user1.setBedId(40002);
         user1.setGender(1);
         Integer result = userMapper.addUser(user1);
         System.out.println("user1 = " + user1.getUserId());
         System.out.println("result = " + result);
         System.out.println("user1.getUserId() = " + user1.getUserId());
     }
     

}
