package com.zer06ix.pms.util;

import java.util.Random;

public class CheckCode {
    public static StringBuilder Code(){
        StringBuilder code = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            code.append(random.nextInt(10));
        }
        return code;
    }
}
