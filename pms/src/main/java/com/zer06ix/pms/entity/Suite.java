package com.zer06ix.pms.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 聪
 */
@Data
@ApiModel("suite 套房对象")
public class Suite {
    @ApiModelProperty(value = "套房id")
    private Integer suiteId;
    @ApiModelProperty(value = "套房对应的楼栋id")
    private Integer buildId;
    @ApiModelProperty(value = "套房类型（0.表示员工宿舍 1.表示商业套房）")
    private Integer suiteType;
    @ApiModelProperty(value = "套房状态（0.已删除的套房 1.可用 2.租满）")
    private Integer suiteStatus;
    @ApiModelProperty(value = "权利人名称")
    private String powername;
    @ApiModelProperty(value = "套房地址信息")
    private String suiteAddress;
    @ApiModelProperty(value = "楼层")
    private Integer floor;
    @ApiModelProperty(value = "使用面积")
    private Double area;
    @ApiModelProperty(value = "开始使用日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date startDate;
    @ApiModelProperty(value = "结束使用日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date endDate;
    @ApiModelProperty(value = "房产证编号")
    private String buildCode;
    @ApiModelProperty(value = "总房间数")
    private Integer roomTotal;
    @ApiModelProperty(value = "剩余房间数")
    private Integer roomLast;
    @ApiModelProperty(value = "评估单价")
    private BigDecimal assessPrice;
    @ApiModelProperty(value = "评估日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date assessStartDate;
    @ApiModelProperty(value = "评估到期日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date assessEndDate;
    @ApiModelProperty(value = "平均单价")
    private BigDecimal averagePrice;
    @ApiModelProperty(value = "备注")
    private String remark;


    public Suite() {
    }

    public Integer getSuiteId() {
        return suiteId;
    }

    public void setSuiteId(Integer suiteId) {
        this.suiteId = suiteId;
    }

    public Integer getBuildId() {
        return buildId;
    }

    public void setBuildId(Integer buildId) {
        this.buildId = buildId;
    }

    public Integer getSuiteType() {
        return suiteType;
    }

    public void setSuiteType(Integer suiteType) {
        this.suiteType = suiteType;
    }

    public Integer getSuiteStatus() {
        return suiteStatus;
    }

    public void setSuiteStatus(Integer suiteStatus) {
        this.suiteStatus = suiteStatus;
    }

    public String getPowername() {
        return powername;
    }

    public void setPowername(String powername) {
        this.powername = powername;
    }

    public String getSuiteAddress() {
        return suiteAddress;
    }

    public void setSuiteAddress(String suiteAddress) {
        this.suiteAddress = suiteAddress;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getBuildCode() {
        return buildCode;
    }

    public void setBuildCode(String buildCode) {
        this.buildCode = buildCode;
    }

    public Integer getRoomTotal() {
        return roomTotal;
    }

    public void setRoomTotal(Integer roomTotal) {
        this.roomTotal = roomTotal;
    }

    public Integer getRoomLast() {
        return roomLast;
    }

    public void setRoomLast(Integer roomLast) {
        this.roomLast = roomLast;
    }

    public BigDecimal getAssessPrice() {
        return assessPrice;
    }

    public void setAssessPrice(BigDecimal assessPrice) {
        this.assessPrice = assessPrice;
    }

    public Date getAssessStartDate() {
        return assessStartDate;
    }

    public void setAssessStartDate(Date assessStartDate) {
        this.assessStartDate = assessStartDate;
    }

    public Date getAssessEndDate() {
        return assessEndDate;
    }

    public void setAssessEndDate(Date assessEndDate) {
        this.assessEndDate = assessEndDate;
    }

    public BigDecimal getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(BigDecimal averagePrice) {
        this.averagePrice = averagePrice;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "Suite{" +
                "suiteId=" + suiteId +
                ", buildId=" + buildId +
                ", suiteType=" + suiteType +
                ", suiteStatus=" + suiteStatus +
                ", powername='" + powername + '\'' +
                ", suiteAddress='" + suiteAddress + '\'' +
                ", floor=" + floor +
                ", area=" + area +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", buildCode='" + buildCode + '\'' +
                ", roomTotal=" + roomTotal +
                ", roomLast=" + roomLast +
                ", assessPrice=" + assessPrice +
                ", assessStartDate=" + assessStartDate +
                ", assessEndDate=" + assessEndDate +
                ", averagePrice=" + averagePrice +
                ", remark='" + remark + '\'' +
                '}';
    }
}
