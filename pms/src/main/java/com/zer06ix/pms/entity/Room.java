package com.zer06ix.pms.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 聪
 */
@ApiModel("room 房间对象")
public class Room {
    @ApiModelProperty(value = "房间id")
    private Integer roomId;
    @ApiModelProperty(value = "套房id")
    private Integer suiteId;
    @ApiModelProperty(value = "楼层")
    private Integer floor;
    @ApiModelProperty(value = "房间名")
    private String roomname;
    @ApiModelProperty(value = "房间地址")
    private String roomAddress;
    @ApiModelProperty(value = "房间面积")
    private double roomArea;
    @ApiModelProperty(value = "房间状态信息(0.被删除 1.可用 2.租满)")
    private Integer roomStatus;
    @ApiModelProperty(value = "房间总床铺")
    private Integer bedTotal;
    @ApiModelProperty(value = "剩余床铺")
    private Integer bedLast;
    @ApiModelProperty(value = "评估单价")
    private BigDecimal assessPrice;
    @ApiModelProperty(value = "评估日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date assessStartDate;
    @ApiModelProperty(value = "评估到期日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date assessEndDate;
    @ApiModelProperty(value = "平均单价")
    private BigDecimal averagePrice;
    @ApiModelProperty(value = "备注")
    private String remark;

    public Room() {
    }

    public String getRoomAddress() {
        return roomAddress;
    }

    public void setRoomAddress(String roomAddress) {
        this.roomAddress = roomAddress;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public Integer getSuiteId() {
        return suiteId;
    }

    public void setSuiteId(Integer suiteId) {
        this.suiteId = suiteId;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public String getRoomname() {
        return roomname;
    }

    public void setRoomname(String roomname) {
        this.roomname = roomname;
    }

    public double getRoomArea() {
        return roomArea;
    }

    public void setRoomArea(double roomArea) {
        this.roomArea = roomArea;
    }

    public Integer getRoomStatus() {
        return roomStatus;
    }

    public void setRoomStatus(Integer roomStatus) {
        this.roomStatus = roomStatus;
    }

    public Integer getBedTotal() {
        return bedTotal;
    }

    public void setBedTotal(Integer bedTotal) {
        this.bedTotal = bedTotal;
    }

    public Integer getBedLast() {
        return bedLast;
    }

    public void setBedLast(Integer bedLast) {
        this.bedLast = bedLast;
    }

    public BigDecimal getAssessPrice() {
        return assessPrice;
    }

    public void setAssessPrice(BigDecimal assessPrice) {
        this.assessPrice = assessPrice;
    }

    public Date getAssessStartDate() {
        return assessStartDate;
    }

    public void setAssessStartDate(Date assessStartDate) {
        this.assessStartDate = assessStartDate;
    }

    public Date getAssessEndDate() {
        return assessEndDate;
    }

    public void setAssessEndDate(Date assessEndDate) {
        this.assessEndDate = assessEndDate;
    }

    public BigDecimal getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(BigDecimal averagePrice) {
        this.averagePrice = averagePrice;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "Room{" +
                "roomId=" + roomId +
                ", suiteId=" + suiteId +
                ", floor=" + floor +
                ", roomname='" + roomname + '\'' +
                ", roomArea=" + roomArea +
                ", roomStatus=" + roomStatus +
                ", bedTotal=" + bedTotal +
                ", bedLast=" + bedLast +
                ", assessPrice=" + assessPrice +
                ", assessStartDate=" + assessStartDate +
                ", assessEndDate=" + assessEndDate +
                ", averagePrice=" + averagePrice +
                ", remark='" + remark + '\'' +
                '}';
    }
}
