package com.zer06ix.pms.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * @author 聪
 */
@ApiModel("houseApply 住房申请表")
public class HouseApply {
    @ApiModelProperty(value = "申请id")
    private Integer applyId;
    @ApiModelProperty(value = "申请日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date applyDate;
    @ApiModelProperty(value = "用户名")
    private String username;
    @ApiModelProperty(value = "用户id")
    private Integer userId;
    @ApiModelProperty(value = "性别")
    private Integer gender;
    @ApiModelProperty(value = "部门id（暂无）")
    private Integer departId;
    @ApiModelProperty(value = "申请状态（0.申请中 1.已同意 2.申请失败）")
    private Integer applyStatus;

    @ApiModelProperty(value = "房间地址")
    private String roomAddress;

    @ApiModelProperty(value = "")
    private Integer applyType;

    @ApiModelProperty(value = "手机号")
    private String telephone;

    @ApiModelProperty(value = "身份证号")
    private String identityNum;
    @ApiModelProperty(value = "银行卡号")
    private String bankNum;
    @ApiModelProperty(value = "原住房信息")
    private String oldSituation;
    @ApiModelProperty(value = "申请原因")
    private String applyReason;
    @ApiModelProperty(value = "房间id")
    private Integer roomId;
    public Integer getDepartId() {
        return departId;
    }
    public void setDepartId(Integer departId) {
        this.departId = departId;
    }

    public String getRoomAddress() {
        return roomAddress;
    }

    public void setRoomAddress(String roomAddress) {
        this.roomAddress = roomAddress;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public Integer getApplyType() {
        return applyType;
    }

    public void setApplyType(Integer applyType) {
        this.applyType = applyType;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    private String remark;

    public HouseApply() {
    }



    public Integer getApplyId() {
        return applyId;
    }

    public void setApplyId(Integer applyId) {
        this.applyId = applyId;
    }

    public Date getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(Date applyDate) {
        this.applyDate = applyDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getDepart() {
        return departId;
    }

    public void setDepart(Integer departId) {
        this.departId = departId;
    }

    public Integer getApplyStatus() {
        return applyStatus;
    }

    public void setApplyStatus(Integer applyStatus) {
        this.applyStatus = applyStatus;
    }

    public String getTelephoneId() {
        return telephone;
    }

    public void setTelephoneId(String telephone) {
        this.telephone = telephone;
    }

    public String getIdentityId() {
        return identityNum;
    }

    public void setIdentityId(String identityId) {
        this.identityNum = identityId;
    }

    public String getBankNum() {
        return bankNum;
    }

    public void setBankNum(String bankNum) {
        this.bankNum = bankNum;
    }

    public String getOldSituation() {
        return oldSituation;
    }

    public void setOldSituation(String oldSituation) {
        this.oldSituation = oldSituation;
    }

    public String getApplyReason() {
        return applyReason;
    }

    public void setApplyReason(String applyReason) {
        this.applyReason = applyReason;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getIdentityNum() {
        return identityNum;
    }

    public void setIdentityNum(String identityNum) {
        this.identityNum = identityNum;
    }
}
