package com.zer06ix.pms.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 龙少2112
 */
@Data
@ApiModel("user 用户对象")
public class User {
    @ApiModelProperty(value = "用户id")
    private Integer userId;
    @ApiModelProperty(value = "用户名")
    private String username;
    @ApiModelProperty(value = "用户密码")
    private String password;
    @ApiModelProperty(value = "电话号码")
    private String phoneNumber;
    @ApiModelProperty(value = "头像信息")
    private String heading;
    @ApiModelProperty(value = "性别")
    private Integer gender;
    @ApiModelProperty(value = "用户状态（0.已注销 1.使用中）")
    private Integer userStatus;
    @ApiModelProperty(value = "角色权限id")
    private Integer roleId;
    @ApiModelProperty(value = "床铺id")
    private Integer bedId;
    @ApiModelProperty(value = "邮箱信息")
    private String email;

    public User(Integer userId, String username, String password, String phoneNumber,
                String heading, Integer gender, Integer userStatus, Integer roleId, Integer bedId, String email) {
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.heading = heading;
        this.gender = gender;
        this.userStatus = userStatus;
        this.roleId = roleId;
        this.bedId = bedId;
        this.email = email;
    }

    public User() {

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getBedId() {
        return bedId;
    }

    public void setBedId(Integer bedId) {
        this.bedId = bedId;
    }



    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", heading='" + heading + '\'' +
                ", gender=" + gender +
                ", userStatus=" + userStatus +
                ", roleId=" + roleId +
                ", bedId=" + bedId +
                ", email='" + email + '\'' +
                '}';
    }
}
