package com.zer06ix.pms.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 聪
 */
@ApiModel( "bed 床铺对象")
public class Bed {
    @ApiModelProperty(value = "bedId主键唯一")
    private Integer bedId;
    @ApiModelProperty(value = "对应的roomID")
    private Integer roomId;
    @ApiModelProperty(value = "bed的名字")
    private String bedname;
    @ApiModelProperty(value = "bed状态（1.可用 2.租满 0.删除）")
    private Integer bedStatus;
    @ApiModelProperty(value = "租户名称对应username")
    private String rentname;
    @ApiModelProperty(value = "入住时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private String resideStartDate;
    @ApiModelProperty(value = "入住截止时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private String resideEndDate;
    @ApiModelProperty(value = "所在套房类型")
    private Integer suiteType;
    @ApiModelProperty(value = "备注")
    private String remark;

    public Bed() {
    }



    public Integer getBedId() {
        return bedId;
    }

    public void setBedId(Integer bedId) {
        this.bedId = bedId;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public String getBedname() {
        return bedname;
    }

    public void setBedname(String bedname) {
        this.bedname = bedname;
    }

    public Integer getBedStatus() {
        return bedStatus;
    }

    public void setBedStatus(Integer bedStatus) {
        this.bedStatus = bedStatus;
    }

    public String getRentname() {
        return rentname;
    }

    public void setRentname(String rentname) {
        this.rentname = rentname;
    }

    public String getResideStartDate() {
        return resideStartDate;
    }

    public void setResideStartDate(String resideStartDate) {
        this.resideStartDate = resideStartDate;
    }

    public String getResideEndDate() {
        return resideEndDate;
    }

    public void setResideEndDate(String resideEndDate) {
        this.resideEndDate = resideEndDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getSuiteType() {
        return suiteType;
    }

    public void setSuiteType(Integer suiteType) {
        this.suiteType = suiteType;
    }

}
