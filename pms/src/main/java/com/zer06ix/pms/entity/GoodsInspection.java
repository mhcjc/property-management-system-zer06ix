package com.zer06ix.pms.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

/**
 * @author 聪
 */
@ApiModel("goodsInspection 物品查验对象")
public class GoodsInspection {

    @ApiModelProperty(value = "物品查验id")
    private Integer inspectionId;
    @ApiModelProperty(value = "退房id")
    private Integer checkOutId;
    @ApiModelProperty(value = "物品名称")
    private String goodsName;
    @ApiModelProperty(value = "物品数量")
    private Integer goodsAmount;
    @ApiModelProperty(value = "物品单价")
    private BigDecimal goodsPrice;
    @ApiModelProperty(value = "物品状态（0.未缴费 1.已缴费 2.无效表单）")
    private Integer goodsStatus;
    @ApiModelProperty(value = "备注")
    private String remark;

    public GoodsInspection() {
    }

    public GoodsInspection(Integer inspectionId, Integer checkOutId, String goodsName, Integer goodsAmount, BigDecimal goodsPrice, Integer goodsStatus, String remark) {
        this.inspectionId = inspectionId;
        this.checkOutId = checkOutId;
        this.goodsName = goodsName;
        this.goodsAmount = goodsAmount;
        this.goodsPrice = goodsPrice;
        this.goodsStatus = goodsStatus;
        this.remark = remark;
    }

    public Integer getInspectionId() {
        return inspectionId;
    }

    public void setInspectionId(Integer inspectionId) {
        this.inspectionId = inspectionId;
    }

    public Integer getCheckOutId() {
        return checkOutId;
    }

    public void setCheckOutId(Integer checkOutId) {
        this.checkOutId = checkOutId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public Integer getGoodsStatus() {
        return goodsStatus;
    }

    public void setGoodsStatus(Integer goodsStatus) {
        this.goodsStatus = goodsStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getGoodsAmount() {
        return goodsAmount;
    }

    public void setGoodsAmount(Integer goodsAmount) {
        this.goodsAmount = goodsAmount;
    }

    @Override
    public String toString() {
        return "GoodsInspection{" +
                "inspectionId=" + inspectionId +
                ", checkOutId=" + checkOutId +
                ", goodsName='" + goodsName + '\'' +
                ", goodsAmount=" + goodsAmount +
                ", goodsPrice=" + goodsPrice +
                ", goodsStatus=" + goodsStatus +
                ", remark='" + remark + '\'' +
                '}';
    }
}
