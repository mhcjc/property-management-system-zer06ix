package com.zer06ix.pms.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 龙少2112
 */
@Data
@ApiModel("wepayment 水电缴费信息")
public class Wepayment {
    @ApiModelProperty(value = "水电缴费信息id")
    private Integer weTableId;
    @ApiModelProperty(value = "支付人姓名")
    private String payername;
    @ApiModelProperty(value = "支付者id对应userId")
    private Integer payerId;
    @ApiModelProperty(value = "支付房间id对应roomId")
    private Integer payRoomId;
    @ApiModelProperty(value = "总电费")
    private BigDecimal electronPrice;
    @ApiModelProperty(value = "总水费")
    private BigDecimal waterPrice;
    @ApiModelProperty(value = "支付开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date payStartDate;
    @ApiModelProperty(value = "支付截止时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date payEndDate;
    @ApiModelProperty(value = "支付方式")
    private String payWay;
    @ApiModelProperty(value = "支付状态（0.未缴费 1.已缴费 2.时效表单）")
    private Integer payStatus;
    @ApiModelProperty(value = "备注")
    private String remark;

    public Wepayment() {
    }

    public Wepayment(Integer weTableId, String payername, Integer payerId, Integer payRoomId, BigDecimal electronPrice, BigDecimal waterPrice, Date payStartDate, Date payEndDate, String payWay, Integer payStatus, String remark) {
        this.weTableId = weTableId;
        this.payername = payername;
        this.payerId = payerId;
        this.payRoomId = payRoomId;
        this.electronPrice = electronPrice;
        this.waterPrice = waterPrice;
        this.payStartDate = payStartDate;
        this.payEndDate = payEndDate;
        this.payWay = payWay;
        this.payStatus = payStatus;
        this.remark = remark;
    }

    public Integer getWeTableId() {
        return weTableId;
    }

    public void setWeTableId(Integer weTableId) {
        this.weTableId = weTableId;
    }

    public String getPayername() {
        return payername;
    }

    public void setPayerName(String payerName) {
        this.payername = payerName;
    }

    public Integer getPayerId() {
        return payerId;
    }

    public void setPayerId(Integer payerId) {
        this.payerId = payerId;
    }

    public Integer getPayRoomId() {
        return payRoomId;
    }

    public void setPayRoomId(Integer payRoomId) {
        this.payRoomId = payRoomId;
    }

    public BigDecimal getElectronPrice() {
        return electronPrice;
    }

    public void setElectronPrice(BigDecimal electronPrice) {
        this.electronPrice = electronPrice;
    }

    public BigDecimal getWaterPrice() {
        return waterPrice;
    }

    public void setWaterPrice(BigDecimal waterPrice) {
        this.waterPrice = waterPrice;
    }

    public Date getPayStartDate() {
        return payStartDate;
    }

    public void setPayStartDate(Date payStartDate) {
        this.payStartDate = payStartDate;
    }

    public Date getPayEndDate() {
        return payEndDate;
    }

    public void setPayEndDate(Date payEndDate) {
        this.payEndDate = payEndDate;
    }

    public String getPayWay() {
        return payWay;
    }

    public void setPayWay(String payWay) {
        this.payWay = payWay;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
