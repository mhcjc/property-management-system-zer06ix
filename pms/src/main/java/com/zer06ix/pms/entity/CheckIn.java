package com.zer06ix.pms.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * @author 聪
 */
@ApiModel("checkin 进场对象")
public class CheckIn {
    @ApiModelProperty(value = "进场主键唯一Id")
    private Integer checkInId;
    @ApiModelProperty(value = "用户名")
    private String username;
    @ApiModelProperty(value = "用户id")
    private Integer userId;
    @ApiModelProperty(value = "房间id")
    private Integer roomId;
    @ApiModelProperty(value = "房间地址")
    private String roomAddress;
    @ApiModelProperty(value = "允许入住时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date resideDate;
    @ApiModelProperty(value = "表单状态(0.未入住 1.已入住 2.入住失败)")
        private Integer resideStatus;
    @ApiModelProperty(value = "允许入住结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private String resideEndDate;
    @ApiModelProperty(value = "备注")
    private String remark;


    public CheckIn() {
    }

    public CheckIn(Integer checkInId, String username, Integer userId, Integer roomId, String roomAddress, Date resideDate, Integer resideStatus, String resideEndDate, String remark) {
        this.checkInId = checkInId;
        this.username = username;
        this.userId = userId;
        this.roomId = roomId;
        this.roomAddress = roomAddress;
        this.resideDate = resideDate;
        this.resideStatus = resideStatus;
        this.resideEndDate = resideEndDate;
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "CheckIn{" +
                "checkInId=" + checkInId +
                ", username='" + username + '\'' +
                ", userId=" + userId +
                ", roomId=" + roomId +
                ", roomAddress='" + roomAddress + '\'' +
                ", resideDate=" + resideDate +
                ", resideStatus=" + resideStatus +
                ", resideEndDate='" + resideEndDate + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }

    public Integer getCheckInId() {
        return checkInId;
    }

    public void setCheckInId(Integer checkInId) {
        this.checkInId = checkInId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public String getRoomAddress() {
        return roomAddress;
    }

    public void setRoomAddress(String roomAddress) {
        this.roomAddress = roomAddress;
    }

    public Date getResideDate() {
        return resideDate;
    }

    public void setResideDate(Date resideDate) {
        this.resideDate = resideDate;
    }

    public Integer getResideStatus() {
        return resideStatus;
    }

    public void setResideStatus(Integer resideStatus) {
        this.resideStatus = resideStatus;
    }

    public String getResideEndDate() {
        return resideEndDate;
    }

    public void setResideEndDate(String resideEndDate) {
        this.resideEndDate = resideEndDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
