package com.zer06ix.pms.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * @author 聪
 */
@ApiModel("checkOut 退场对象")
public class CheckOut {
    @ApiModelProperty(value = "退场id")
    private Integer checkOutId;
    @ApiModelProperty(value = "用户名")
    private String username;
    @ApiModelProperty(value = "用户id")
    private Integer userId;
    @ApiModelProperty(value = "房间id")
    private Integer roomId;
    @ApiModelProperty(value = "房间地址")
    private String roomAddress;
    @ApiModelProperty(value = "入住时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date resideDate;
    @ApiModelProperty(value = "开始退场时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date leaveStartDate;
    @ApiModelProperty(value = "退场截至时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date leaveEndDate;
    @ApiModelProperty(value = "0.未退场 1.申请中 2.退场失败")
    private Integer leaveStatus;
    @ApiModelProperty(value = "备注")
    private String remark;

    public CheckOut(Integer checkOutId, String username, Integer userId, Integer roomId, String roomAddress, Date resideDate, Date leaveStartDate, Date leaveEndDate, Integer leaveStatus, String remark) {
        this.checkOutId = checkOutId;
        this.username = username;
        this.userId = userId;
        this.roomId = roomId;
        this.roomAddress = roomAddress;
        this.resideDate = resideDate;
        this.leaveStartDate = leaveStartDate;
        this.leaveEndDate = leaveEndDate;
        this.leaveStatus = leaveStatus;
        this.remark = remark;
    }

    public CheckOut() {
    }

    public Integer getCheckOutId() {
        return checkOutId;
    }

    public void setCheckOutId(Integer checkOutId) {
        this.checkOutId = checkOutId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public String getRoomAddress() {
        return roomAddress;
    }

    public void setRoomAddress(String roomAddress) {
        this.roomAddress = roomAddress;
    }

    public Date getResideDate() { return resideDate; }

    public void setResideDate(Date resideDate) { this.resideDate = resideDate; }

    public Date getLeaveStartDate() {
        return leaveStartDate;
    }

    public void setLeaveStartDate(Date leaveStartDate) {
        this.leaveStartDate = leaveStartDate;
    }

    public Date getLeaveEndDate() {
        return leaveEndDate;
    }

    public void setLeaveEndDate(Date leaveEndDate) {
        this.leaveEndDate = leaveEndDate;
    }

    public Integer getLeaveStatus() {
        return leaveStatus;
    }

    public void setLeaveStatus(Integer leaveStatus) {
        this.leaveStatus = leaveStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "CheckOut{" +
                "checkOutId=" + checkOutId +
                ", username='" + username + '\'' +
                ", userId=" + userId +
                ", roomId=" + roomId +
                ", roomAddress='" + roomAddress + '\'' +
                ", resideDate=" + resideDate +
                ", leaveStartDate=" + leaveStartDate +
                ", leaveEndDate=" + leaveEndDate +
                ", leaveStatus=" + leaveStatus +
                ", remark='" + remark + '\'' +
                '}';
    }
}
