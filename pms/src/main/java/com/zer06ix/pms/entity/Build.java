package com.zer06ix.pms.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 聪
 */
@ApiModel("build 楼栋对象")
public class Build {
    @ApiModelProperty(value = "build对象")
    private Integer buildId;
    @ApiModelProperty(value = "对应的项目projectId")
    private Integer projectId;
    @ApiModelProperty(value = "楼栋名称")
    private String buildname;
    @ApiModelProperty(value = "房产权利人")
    private String buildOwner;
    @ApiModelProperty(value = "房产物业详细信息")
    private String propertyAddress;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(value = "开始使用日期")
    private Date startDate;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(value = "结束使用时间")
    private Date endDate;
    @ApiModelProperty(value = "楼栋状态(0.删除 1.可用 2.租满)")
    private Integer buildStatus;
    @ApiModelProperty(value = "楼栋下的套房总数")
    private Integer suiteTotal;
    @ApiModelProperty(value = "剩余套房数")
    private Integer suiteLast;
    @ApiModelProperty(value = "评估单价")
    private BigDecimal assessPrice;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(value = "评估日期")
    private Date assessStartDate;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(value = "评估到期日期")
    private Date assessEndDate;
    @ApiModelProperty(value = "平均单价")
    private BigDecimal averagePrice;
    @ApiModelProperty(value = "备注")
    private String remark;

    public Build() {
    }

    public Integer getBuildId() {
        return buildId;
    }

    public void setBuildId(Integer buildId) {
        this.buildId = buildId;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getBuildname() {
        return buildname;
    }

    public void setBuildname(String buildname) {
        this.buildname = buildname;
    }

    public String getBuildOwner() {
        return buildOwner;
    }

    public void setBuildOwner(String buildOwner) {
        this.buildOwner = buildOwner;
    }

    public String getPropertyAddress() {
        return propertyAddress;
    }

    public void setPropertyAddress(String propertyAddress) {
        this.propertyAddress = propertyAddress;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getBuildStatus() {
        return buildStatus;
    }

    public void setBuildStatus(Integer buildStatus) {
        this.buildStatus = buildStatus;
    }

    public Integer getSuiteTotal() {
        return suiteTotal;
    }

    public void setSuiteTotal(Integer suiteTotal) {
        this.suiteTotal = suiteTotal;
    }

    public Integer getSuiteLast() {
        return suiteLast;
    }

    public void setSuiteLast(Integer suiteLast) {
        this.suiteLast = suiteLast;
    }

    public BigDecimal getAssessPrice() {
        return assessPrice;
    }

    public void setAssessPrice(BigDecimal assessPrice) {
        this.assessPrice = assessPrice;
    }

    public Date getAssessStartDate() {
        return assessStartDate;
    }

    public void setAssessStartDate(Date assessStartDate) {
        this.assessStartDate = assessStartDate;
    }

    public Date getAssessEndDate() {
        return assessEndDate;
    }

    public void setAssessEndDate(Date assessEndDate) {
        this.assessEndDate = assessEndDate;
    }

    public BigDecimal getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(BigDecimal averagePrice) {
        this.averagePrice = averagePrice;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "Build{" +
                "buildId=" + buildId +
                ", projectId=" + projectId +
                ", buildname='" + buildname + '\'' +
                ", buildOwner='" + buildOwner + '\'' +
                ", propertyAddress='" + propertyAddress + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", buildStatus=" + buildStatus +
                ", suiteTotal=" + suiteTotal +
                ", suiteLast=" + suiteLast +
                ", assessPrice=" + assessPrice +
                ", assessStartDate=" + assessStartDate +
                ", assessEndDate=" + assessEndDate +
                ", averagePrice=" + averagePrice +
                ", remark='" + remark + '\'' +
                '}';
    }
}
