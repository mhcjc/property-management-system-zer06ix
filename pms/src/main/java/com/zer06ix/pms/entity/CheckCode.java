package com.zer06ix.pms.entity;

import lombok.Data;

@Data
public class CheckCode {
    Integer checkId;
    String email;
    String CheckCode;
    Integer checkCodeStatus;

    public CheckCode() {
    }

    public CheckCode(Integer checkId, String email, String checkCode, Integer checkCodeStatus) {
        this.checkId = checkId;
        this.email = email;
        CheckCode = checkCode;
        this.checkCodeStatus = checkCodeStatus;
    }

    public Integer getCheckId() {
        return checkId;
    }

    public void setCheckId(Integer checkId) {
        this.checkId = checkId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCheckCode() {
        return CheckCode;
    }

    public void setCheckCode(String checkCode) {
        CheckCode = checkCode;
    }

    public Integer getCheckCodeStatus() {
        return checkCodeStatus;
    }

    public void setCheckCodeStatus(Integer checkCodeStatus) {
        this.checkCodeStatus = checkCodeStatus;
    }
}
