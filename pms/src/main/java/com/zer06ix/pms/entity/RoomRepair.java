package com.zer06ix.pms.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author 龙少2112
 */
@Data
@ApiModel("roomRepair 房间报修对象")
public class RoomRepair {
    @ApiModelProperty(value = "报修id")
    private Integer tableId;
    @ApiModelProperty(value = "申请人姓名对应username")
    private String applicant;
    @ApiModelProperty(value = "申请人id对应userId")
    private Integer applicantId;
    @ApiModelProperty(value = "房间Id")
    private Integer roomId;
    @ApiModelProperty(value = "申请人电话 对应phoneNum")
    private String applicantPhone;
    @ApiModelProperty(value = "具体维修位置")
    private String repairPlace;
    @ApiModelProperty(value = "预约维修时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date repairDate;
    @ApiModelProperty(value = "维修状态(0.未维修 1.已维修 2.已失效)")
    private Integer repairStatus;
    @ApiModelProperty(value = "备注")
    private String remark;

    public RoomRepair() {

    }

    public RoomRepair(Integer tableId, String applicant, Integer applicantId, Integer roomId, String applicantPhone, String repairPlace, Date repairDate, Integer repairStatus, String remark) {
        this.tableId = tableId;
        this.applicant = applicant;
        this.applicantId = applicantId;
        this.roomId = roomId;
        this.applicantPhone = applicantPhone;
        this.repairPlace = repairPlace;
        this.repairDate = repairDate;
        this.repairStatus = repairStatus;
        this.remark = remark;
    }

    public Integer getTableId() {
        return tableId;
    }

    public void setTableId(Integer tableId) {
        this.tableId = tableId;
    }

    public String getApplicant() {
        return applicant;
    }

    public void setApplicant(String applicant) {
        this.applicant = applicant;
    }

    public Integer getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(Integer applicantId) {
        this.applicantId = applicantId;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public String getApplicantPhone() {
        return applicantPhone;
    }

    public void setApplicantPhone(String applicantPhone) {
        this.applicantPhone = applicantPhone;
    }

    public String getRepairPlace() {
        return repairPlace;
    }

    public void setRepairPlace(String repairPlace) {
        this.repairPlace = repairPlace;
    }

    public Date getRepairDate() {
        return repairDate;
    }

    public void setRepairDate(Date repairDate) {
        this.repairDate = repairDate;
    }

    public Integer getRepairStatus() {
        return repairStatus;
    }

    public void setRepairStatus(Integer repairStatus) {
        this.repairStatus = repairStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
