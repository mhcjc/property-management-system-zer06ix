package com.zer06ix.pms.entity;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel("indexInfo 主页信息对象")
public class IndexInfo {
    private Integer textId;
    private String partyBuilding;
    private String notice;
    private String encyclopedias;
    private Date date;
    private Integer textStatus;

    public IndexInfo() {
    }

    public IndexInfo(Integer textId, String partyBuilding, String notice, String encyclopedias, Date date, Integer textStatus) {
        this.textId = textId;
        this.partyBuilding = partyBuilding;
        this.notice = notice;
        this.encyclopedias = encyclopedias;
        this.date = date;
        this.textStatus = textStatus;
    }

    public Integer getTextId() {
        return textId;
    }

    public void setTextId(Integer textId) {
        this.textId = textId;
    }

    public String getPartyBuilding() {
        return partyBuilding;
    }

    public void setPartyBuilding(String partyBuilding) {
        this.partyBuilding = partyBuilding;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public String getEncyclopedias() {
        return encyclopedias;
    }

    public void setEncyclopedias(String encyclopedias) {
        this.encyclopedias = encyclopedias;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getTextStatus() {
        return textStatus;
    }

    public void setTextStatus(Integer textStatus) {
        this.textStatus = textStatus;
    }
}
