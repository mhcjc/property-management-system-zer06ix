package com.zer06ix.pms.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author 聪
 */
@ApiModel("project 项目对象")
public class Project {
    @ApiModelProperty(value = "项目id")
    private Integer projectId;
    @ApiModelProperty(value = "项目名称")
    private String projectname;
    @ApiModelProperty(value = "项目类型")
    private Integer projectType;
    @ApiModelProperty(value = "项目地址")
    private String projectAddress;
    @ApiModelProperty(value = "项目开始日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date startDate;
    @ApiModelProperty(value = "项目结束日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date endDate;
    @ApiModelProperty(value = "项目状态（0.被删除 1.可用）")
    private Integer projectStatus;
    @ApiModelProperty(value = "项目中的总楼栋数")
    private Integer buildTotal;
    @ApiModelProperty(value = "项目中剩余的楼栋数")
    private Integer buildLast;
    @ApiModelProperty(value = "备注")
    private String remark;

    public Project() {
    }

    @Override
    public String toString() {
        return "Project{" +
                "projectId=" + projectId +
                ", projectname='" + projectname + '\'' +
                ", projectType=" + projectType +
                ", projectAddress='" + projectAddress + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", projectStatus=" + projectStatus +
                ", buildTotal=" + buildTotal +
                ", buildLast=" + buildLast +
                ", remark='" + remark + '\'' +
                '}';
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getProjectname() {
        return projectname;
    }

    public void setProjectname(String projectname) {
        this.projectname = projectname;
    }

    public Integer getProjectType() {
        return projectType;
    }

    public void setProjectType(Integer projectType) {
        this.projectType = projectType;
    }

    public String getProjectAddress() {
        return projectAddress;
    }

    public void setProjectAddress(String projectAddress) {
        this.projectAddress = projectAddress;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(Integer projectStatus) {
        this.projectStatus = projectStatus;
    }

    public Integer getBuildTotal() {
        return buildTotal;
    }

    public void setBuildTotal(Integer buildTotal) {
        this.buildTotal = buildTotal;
    }

    public Integer getBuildLast() {
        return buildLast;
    }

    public void setBuildLast(Integer buildLast) {
        this.buildLast = buildLast;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Project(Integer projectId, String projectname, Integer projectType, String projectAddress, Date startDate, Date endDate, Integer projectStatus, Integer buildTotal, Integer buildLast, String remark) {
        this.projectId = projectId;
        this.projectname = projectname;
        this.projectType = projectType;
        this.projectAddress = projectAddress;
        this.startDate = startDate;
        this.endDate = endDate;
        this.projectStatus = projectStatus;
        this.buildTotal = buildTotal;
        this.buildLast = buildLast;
        this.remark = remark;
    }
}
