package com.zer06ix.pms.exception;

import com.zer06ix.pms.enums.ResultCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 聪
 *
 * 全局异常捕获
 */
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Map<String, Object> erro(Exception e) {
        Map<String, Object> map = new HashMap<>();
        e.printStackTrace();
        log.error(e.getMessage(), e);
        map.put("data",0);
        map.put("status", ResultCode.COMMON_FAIL.getCode());
        map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        return map;
    }
}
