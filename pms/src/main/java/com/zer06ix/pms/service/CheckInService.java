package com.zer06ix.pms.service;

import com.zer06ix.pms.entity.CheckIn;
import com.zer06ix.pms.entity.User;

import java.util.List;

/**
 * @author 聪
 */
public interface CheckInService {
    List<CheckIn> queryCheckIn(User user);

    CheckIn addCheckIn(CheckIn checkIn);

    CheckIn cancelCheckIn(CheckIn checkIn);

    int approveCheckIn(Integer userId, Integer status);

}
