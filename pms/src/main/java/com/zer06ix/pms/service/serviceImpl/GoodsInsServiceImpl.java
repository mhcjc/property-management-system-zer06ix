package com.zer06ix.pms.service.serviceImpl;

import com.zer06ix.pms.entity.CheckOut;
import com.zer06ix.pms.entity.GoodsInspection;
import com.zer06ix.pms.entity.Room;
import com.zer06ix.pms.mapper.CheckInMapper;
import com.zer06ix.pms.mapper.GoodInspectionMapper;
import com.zer06ix.pms.service.GoodsInsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author 浩
 */

@Service
@Transactional(rollbackFor = RuntimeException.class)
public class GoodsInsServiceImpl implements GoodsInsService {

    @Autowired
    private GoodInspectionMapper goodInspectionMapper;

    @Override
    public Integer addGoodInspection(GoodsInspection goodsInspection){
        Integer account = 0;
        Integer i = goodInspectionMapper.addGoodInspection(goodsInspection);
        account = account + i;
        return account;
    }

    @Override
    public Integer updateGoodsInsStatus(Integer inspectionId){
        Integer flag = goodInspectionMapper.updateGoodsInsStatus(inspectionId);
        return flag;
    }

    @Override
    public Integer updateRoomGoodsInsStatus(Integer checkOutId){
        Integer flag = goodInspectionMapper.updateRoomGoodsInsStatus(checkOutId);
        return flag;
    }

    @Override
    public BigDecimal account(Integer checkOutId){
        BigDecimal goods = new BigDecimal("0");
        BigDecimal roomprice = new BigDecimal("0");
        BigDecimal totalprice = new BigDecimal("0");
        List<GoodsInspection> goodsInspections = goodInspectionMapper.queryGoodsInsBycheckOutId(checkOutId);
        for (GoodsInspection goodsInspection : goodsInspections) {
            goods = goods.add(goodsInspection.getGoodsPrice());
        }
        Room room = goodInspectionMapper.queryRoomByCheckoutId(checkOutId);
        roomprice = room.getAssessPrice();
        CheckOut checkOut = goodInspectionMapper.queryCheckout(checkOutId);
        long l = (checkOut.getLeaveEndDate().getTime() - checkOut.getResideDate().getTime()) / (24 * 60 * 60 * 1000);
        BigDecimal daytime = new BigDecimal(l);
        totalprice = goods.add(roomprice.multiply(daytime));
        return totalprice;
    }

}
