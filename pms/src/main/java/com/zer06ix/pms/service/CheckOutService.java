package com.zer06ix.pms.service;

import com.zer06ix.pms.entity.CheckOut;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 龙少2112
 */
@Service
public interface CheckOutService {
    /**
     * 用于添加退租申请
     * @param checkOut
     * @return 成功插入的行数
     */
    Integer insertCheckOut(CheckOut checkOut);

    /**
     * 用于更改退租时间
     * @param checkOut
     * @return 成功更改的行数
     */
    Integer updateCheckOutByUserId(CheckOut checkOut);

    /**
     * 用于查询正在处理的申请
     * @param userId
     * @return 申请信息
     */
    CheckOut queryCheckOutByUserId(Integer userId);

    /**
     * 用于更改申请状态
     * @param userId
     * @return
     */
    Integer updateCheckOutStatus(Integer userId, Integer checkOutStatus);

    List<CheckOut> checkOutAll();

    Integer cheeckOutTotal();

    Integer queryCheckOutIdByRoomId(Integer roomId);


}
