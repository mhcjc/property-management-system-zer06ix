package com.zer06ix.pms.service.serviceImpl;

import com.zer06ix.pms.entity.*;
import com.zer06ix.pms.mapper.*;
import com.zer06ix.pms.service.HouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 聪
 */
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class HouseServiceImpl implements HouseService {
    @Autowired
    private BedMapper bedMapper;

    @Autowired
    private BuildMapper buildMapper;

    @Autowired
    private ProjectMapper projectMapper;

    @Autowired
    private RoomMapper roomMapper;

    @Autowired
    private SuiteMapper suiteMapper;

    @Autowired
    private HouseApplyMapper houseApplyMapper;

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<Project> queryAllProject() {
        List<Project> projects = projectMapper.queryAllProjects();
        return projects;
    }

    @Override
    public List<Bed> queryBedByRoomId(Integer roomId) {
        List<Bed> beds = bedMapper.queryBedByRoomId(roomId);
        return beds;
    }

    @Override
    public List<Room> queryRoomsBySuiteId(Integer suiteId) {
        List<Room> Rooms = roomMapper.queryRoomBySuiteId(suiteId);
        return Rooms;
    }

    @Override
    public List<Suite> querySuiteByBuildId(Integer buildId) {
        List<Suite> suites = suiteMapper.querySuiteByBuildId(buildId);
        return suites;
    }

    @Override
    public List<Build> queryBuildByProjectId(Integer projectId) {
        List<Build> builds = buildMapper.queryBuildByProjectId(projectId);
        return builds;
    }

    @Override
    public List<Bed> queryBedByBuildId(Integer buildId) {
        List<Bed> beds = new ArrayList<>();
        List<Suite> suites = suiteMapper.querySuiteByBuildId(buildId);
        for (Suite suite : suites) {
            List<Room> rooms = roomMapper.queryRoomBySuiteId(suite.getSuiteId());
            for (Room room : rooms) {
                beds.addAll(bedMapper.queryBedByRoomId(room.getRoomId()));
            }
        }
        return beds;
    }

    @Override
    public List<Bed> queryBedBySuiteId(Integer suiteId) {
        List<Bed> beds = new ArrayList<>();
        List<Room> rooms = roomMapper.queryRoomBySuiteId(suiteId);
        for (Room room : rooms) {
            beds.addAll(bedMapper.queryBedByRoomId(room.getRoomId()));
        }
        return beds;
    }

    @Override
    public List<Bed> queryBedByProjectId(Integer projectId) {
        List<Bed> beds = new ArrayList<>();
        List<Build> builds = buildMapper.queryBuildByProjectId(projectId);
        for (Build build : builds) {
            List<Suite> suites = suiteMapper.querySuiteByBuildId(build.getBuildId());
            for (Suite suite : suites) {
                List<Room> rooms = roomMapper.queryRoomBySuiteId(suite.getSuiteId());
                for (Room room : rooms) {
                    beds.addAll(bedMapper.queryBedByRoomId(room.getRoomId()));
                }
            }
        }
        return beds;
    }

    @Override
    public List<Room> queryAllRoom() {
        return roomMapper.queryAllRoom();
    }

    @Override
    public List<Room> queryRoomByBuildId(Integer buildId) {
        List<Room> rooms = new ArrayList<>();
            List<Suite> suites = suiteMapper.querySuiteByBuildId(buildId);
            for (Suite suite : suites) {
                List<Room> tempRooms = roomMapper.queryRoomBySuiteId(suite.getSuiteId());
                for (Room room : tempRooms) {
                    rooms.add(room);
                }
            }
        return rooms;
    }

    @Override
    public List<Room> queryRoomBySuiteId(Integer suiteId) {
        return  roomMapper.queryRoomBySuiteId(suiteId);
    }

    @Override
    public List<Room> queryRoomByProjectId(Integer projectId) {
        List<Room> rooms = new ArrayList<>();
        List<Build> builds = buildMapper.queryBuildByProjectId(projectId);
        for (Build build : builds) {
            List<Suite> suites = suiteMapper.querySuiteByBuildId(build.getBuildId());
            for (Suite suite : suites) {
                List<Room> tempRooms = roomMapper.queryRoomBySuiteId(suite.getSuiteId());
                for (Room room : tempRooms) {
                    rooms.add(room);
                }
            }
        }
        System.out.println("rooms = " + rooms);
        return rooms;
    }

    @Override
    public List<Bed> queryAllBed() {
        return bedMapper.queryAllBed();
    }

    @Override
    public int addHouseApply(HouseApply houseApply) {
        Integer roomId = houseApply.getRoomId();
        Room room = roomMapper.queryRoomById(roomId);
        houseApply.setApplyStatus(3);
        Integer result = houseApplyMapper.addHouseApply(houseApply);

        return result;
    }

    @Override
    public HouseApply cancelHouseApply(HouseApply houseApply) {
        Integer roomId = houseApply.getRoomId();
        Room room = roomMapper.queryRoomById(roomId);
        if (room.getBedLast() >0) {
            room.setBedLast(room.getBedLast() + 1);
            roomMapper.updateRoom(room);
        }
        houseApply.setApplyStatus(0);
        Integer result = houseApplyMapper.updateHouseApply(houseApply);
        if (result == 0) {
            return null;
        }
        return houseApply;
    }

    @Override
    public Room queryRoomByRoomId(Integer roomId) {
        Room room = roomMapper.queryRoomById(roomId);
        return room;
    }

    @Override
    public HouseApply approveHouseApply(HouseApply houseApply) {
        houseApply.setApplyStatus(1);
        Integer result = userMapper.updateUserRoomId(houseApply.getRoomId(), houseApply.getUserId());
        if (result == 0) {
            return null;
        }
        Integer integer = houseApplyMapper.updateHouseApply(houseApply);
        return houseApply;
    }

    @Override
    public  List<HouseApply> queryHouseApplyByUserId(Integer userId) {
        List<HouseApply> houseApplies = houseApplyMapper.queryHouseApplyByUserId(userId);
        return houseApplies;
    }

    @Override
    public  List<HouseApply> queryAllHouseApply() {
        List<HouseApply> houseApplies = houseApplyMapper.queryAllHouseApply();
        return houseApplies;
    }


}
