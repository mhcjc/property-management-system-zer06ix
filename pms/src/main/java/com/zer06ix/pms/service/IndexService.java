package com.zer06ix.pms.service;

import com.zer06ix.pms.entity.IndexInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 龙少2112
 */
@Service
public interface IndexService {
    public Integer insertIndex(IndexInfo indexInfo);

    public List<IndexInfo> queryIndex();

    public Integer updateIndex(IndexInfo indexInfo);

    public Integer deleteIndex(Integer textId);

    public Integer queryIndexTotal();

    public List<IndexInfo> queryIndexLimit(Integer start,Integer end);
}
