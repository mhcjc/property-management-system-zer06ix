package com.zer06ix.pms.service.serviceImpl;

import com.zer06ix.pms.entity.CheckOut;
import com.zer06ix.pms.mapper.CheckOutMapper;
import com.zer06ix.pms.service.CheckOutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional(rollbackFor = RuntimeException.class)
public class CheckOutServiceImpl implements CheckOutService {

    @Autowired
    CheckOutMapper checkOutMapper;

    @Override
    public Integer insertCheckOut(CheckOut checkOut) {
        Integer flag = checkOutMapper.insertCheckOut(checkOut);
        return flag;
    }

    @Override
    public Integer updateCheckOutByUserId(CheckOut checkOut) {
        Integer flag = checkOutMapper.updateCheckOutByUserId(checkOut);
        return flag;
    }

    @Override
    public CheckOut queryCheckOutByUserId(Integer userId) {
        CheckOut checkOut = checkOutMapper.queryCheckOutByUserId(userId);
        return checkOut;
    }

    @Override
    public Integer updateCheckOutStatus(Integer userId, Integer checkOutStatus) {
        Integer flag = checkOutMapper.updateCheckOutStatus(userId,checkOutStatus);
        return flag;
    }

    @Override
    public List<CheckOut> checkOutAll() {
        List<CheckOut> checkOuts = checkOutMapper.queryCheckOutAll();
        return checkOuts;
    }

    @Override
    public Integer cheeckOutTotal() {
        Integer total = checkOutMapper.queryCheckOutTotal();
        return total;
    }

    @Override
    public Integer queryCheckOutIdByRoomId(Integer roomId) {
        Integer checkOutId = checkOutMapper.queryCheckOutIdByRoomId(roomId);
        return checkOutId;
    }
}
