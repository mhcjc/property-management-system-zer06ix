package com.zer06ix.pms.service;

import org.springframework.stereotype.Service;

@Service
public interface MailService {
    public String sendMail(String useremail);
}
