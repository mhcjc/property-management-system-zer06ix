package com.zer06ix.pms.service.serviceImpl;

import com.zer06ix.pms.entity.IndexInfo;
import com.zer06ix.pms.mapper.IndexInfoMapper;
import com.zer06ix.pms.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 龙少2112
 */
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class IndexServiceImpl implements IndexService {

    @Autowired
    IndexInfoMapper indexInfoMapper;

    @Override
    public Integer insertIndex(IndexInfo indexInfo) {
        System.out.println("index = " + indexInfo);
        Integer flag = indexInfoMapper.insertIndex(indexInfo);
        return flag;
    }

    @Override
    public List<IndexInfo> queryIndex() {
        List<IndexInfo> indices = indexInfoMapper.queryIndex();
        return indices;
    }

    @Override
    public Integer updateIndex(IndexInfo indexInfo) {
        Integer flag = indexInfoMapper.updateIndexByTextId(indexInfo);
        return flag;
    }

    @Override
    public Integer deleteIndex(Integer textId) {
        Integer flag = indexInfoMapper.deleteIndexByTextId(textId);
        return flag;
    }

    @Override
    public Integer queryIndexTotal() {
        Integer total = indexInfoMapper.queryIndexTotal();
        return total;
    }

    @Override
    public List<IndexInfo> queryIndexLimit(Integer start, Integer end) {
        List<IndexInfo> indexInfos = indexInfoMapper.queryIndexLimit(start,end);
        return indexInfos;
    }
}
