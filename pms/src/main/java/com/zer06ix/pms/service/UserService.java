package com.zer06ix.pms.service;

import com.zer06ix.pms.entity.User;
import org.springframework.stereotype.Service;

/**
 * @author 龙少2112
 */
@Service
public interface UserService {
    public User isUsernameExist(Integer userId);

    public User login(Integer userId, String password);

    public User signUp(User user);

    public Integer forgetPassword(Integer userId, String newPassword);

    public Integer changePassword(Integer userId, String oldPassword, String newPassword);

    public Integer changeStatus(Integer userId, Integer status);

}

