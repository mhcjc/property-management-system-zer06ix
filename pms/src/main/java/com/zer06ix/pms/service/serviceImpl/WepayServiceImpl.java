package com.zer06ix.pms.service.serviceImpl;

import com.zer06ix.pms.entity.Wepayment;
import com.zer06ix.pms.mapper.WepayMapper;
import com.zer06ix.pms.service.WepayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor = RuntimeException.class)
public class WepayServiceImpl implements WepayService {

    @Autowired
    private WepayMapper wepayMapper;
    
    @Override
    public List<Wepayment> expensesInfo(Integer roomId,Integer status) {
        List<Wepayment> wepayment = wepayMapper.queryWepaymentByRoomId(roomId,status);
        return wepayment;
    }

    @Override
    public Integer paySuccess(Integer roomId, String payWay) {
        Integer flag = wepayMapper.updateWepaymentByRoomId(roomId,payWay);
        return flag;
    }

    @Override
    public List<Wepayment> expensesInfoLimit(Integer start, Integer end) {
        List<Wepayment> wepayments = wepayMapper.queryWepaymentLimit(start,end);
        return wepayments;
    }

    @Override
    public Integer expensesInfoTotal() {
        Integer total = wepayMapper.queryWepaymentTotal();
        return total;
    }
}
