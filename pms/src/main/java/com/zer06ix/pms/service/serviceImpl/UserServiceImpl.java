package com.zer06ix.pms.service.serviceImpl;

import com.zer06ix.pms.entity.User;
import com.zer06ix.pms.mapper.UserMapper;
import com.zer06ix.pms.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author 龙少2112
 */
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    /**
     * 用户名验证
     * @param userId
     * @return flag标签
     */
    @Override
    public User isUsernameExist(Integer userId) {
        User user = userMapper.queryUserById(userId);
        return user;
    }

    /**
     * 登录验证
     * @param userId
     * @param password
     * @return user对象
     */
    @Override
    public User login(Integer userId, String password) {
        User user = userMapper.queryUserById(userId);
        if(user.getUserId().equals(userId) && user.getPassword().equals(password)){
            user.setPassword("");
        }else{
            user = null;
        }
        return user;
    }

    @Override
    public User signUp(User user) {
        User result = userMapper.queryUserByPhoneNumber(user.getPhoneNumber());
        if (result == null) {
            userMapper.addUser(user);
            return user;
        }else {
            return null;
        }
    }

    @Override
    public Integer forgetPassword(Integer userId, String newPassword) {
        Integer flag = userMapper.updateUserById(userId, newPassword);
        return flag;
    }

    @Override
    public Integer changePassword(Integer userId, String oldPassword, String newPassword) {
        Integer flag =  0;
        User user = userMapper.queryUserById(userId);
        if(user.getPassword().equals(oldPassword)){
            userMapper.updateUserById(userId,newPassword);
            flag = 1;
        }
        return flag;
    }

    @Override
    public Integer changeStatus(Integer userId,Integer status) {
        Integer flag = userMapper.updateUserStatus(userId,status);
        return flag;
    }
}
