package com.zer06ix.pms.service;

import com.zer06ix.pms.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 聪
 */
@Service
public interface HouseService {
    /**
     *
     * @return
     */
    List<Project> queryAllProject();

    /**
     * 查询Bed通过roomId
     * @param roomId
     * @return
     */
    List<Bed> queryBedByRoomId(Integer roomId);


    /**
     * 查询room通过suiteId
     * @param suiteId
     * @return
     */
    List<Room> queryRoomsBySuiteId(Integer suiteId);

    /**
     * 查询Suite通过buildId
     * @param buildId
     * @return
     */
    List<Suite> querySuiteByBuildId(Integer buildId);

    /**
     * 查询Build
     * @param projectId
     * @return
     */
    List<Build> queryBuildByProjectId(Integer projectId);

    /**
     * 通过buildId查询Bed
     * @param buildId
     * @return
     */
    List<Bed> queryBedByBuildId(Integer buildId);

    /**
     * 通过suiteId查询Bed
     * @param suiteId
     * @return
     */
    List<Bed> queryBedBySuiteId(Integer suiteId);

    /**
     * 通过projectId查询Bed
     * @param projectId
     * @return
     */
    List<Bed> queryBedByProjectId(Integer projectId);

    /**
     * 查询所有的Bed
     * @return
     */
    List<Room> queryAllRoom();
/**
     * 通过buildId查询Bed
     * @param buildId
     * @return
     */
    List<Room> queryRoomByBuildId(Integer buildId);

    /**
     * 通过suiteId查询Bed
     * @param suiteId
     * @return
     */
    List<Room> queryRoomBySuiteId(Integer suiteId);

    /**
     * 通过projectId查询Bed
     * @param projectId
     * @return
     */
    List<Room> queryRoomByProjectId(Integer projectId);

    /**
     * 查询所有的Bed
     * @return
     */
    List<Bed> queryAllBed();

    /**
     * 申请房间
     * @param houseApply
     * @return
     */
    int addHouseApply(HouseApply houseApply);

    /**
     * 取消住房申请
     * @param houseApply
     * @return
     */
    HouseApply cancelHouseApply(HouseApply houseApply);

    Room queryRoomByRoomId(Integer roomId);

    HouseApply approveHouseApply(HouseApply houseApply);

    List<HouseApply> queryHouseApplyByUserId(Integer userId);

    List<HouseApply> queryAllHouseApply();




}
