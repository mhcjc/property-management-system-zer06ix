package com.zer06ix.pms.service.serviceImpl;

import com.zer06ix.pms.entity.CheckIn;
import com.zer06ix.pms.entity.User;
import com.zer06ix.pms.enums.CheckInStatus;
import com.zer06ix.pms.enums.UserTypeEnum;
import com.zer06ix.pms.mapper.CheckInMapper;
import com.zer06ix.pms.mapper.HouseApplyMapper;
import com.zer06ix.pms.service.CheckInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 聪
 */
@Service
@Transactional(rollbackFor = RuntimeException.class)
public class CheckInServiceImpl implements CheckInService {

    @Autowired
    private CheckInMapper checkInMapper;

    @Autowired
    private HouseApplyMapper houseApplyMapper;

    @Override
    public List<CheckIn> queryCheckIn(User user){
        List<CheckIn> checkIns = null;
        if (user.getRoleId() == UserTypeEnum.SYSTEM_ADMIN.getTypeCode()
                || user.getRoleId() == UserTypeEnum.SYSTEM_SDM.getTypeCode()){
            checkIns = checkInMapper.queryAllCheckIn();
        }else {
            checkIns = checkInMapper.queryCheckInByUserId(user.getUserId());
        }
        return checkIns;
    }

    @Override
    public CheckIn addCheckIn(CheckIn checkIn) {
        List<CheckIn> tempCheckIns = checkInMapper.queryCheckInByUserId(checkIn.getUserId());
        if (tempCheckIns.isEmpty()) {
            System.out.println("--------------------------------"+checkIn);
            checkInMapper.addCheckIn(checkIn);
            return checkIn;
        }else {
            return null;
        }
    }

    @Override
    public CheckIn cancelCheckIn(CheckIn checkIn) {
        CheckIn temp = checkInMapper.queryCheckInByCheckInId(checkIn.getCheckInId());
        if (temp.getUserId().equals(checkIn.getUserId())) {
            checkIn.setResideStatus(CheckInStatus.RESIDE_STATUS_CANCEL.getCode());
            checkInMapper.cancelCheckIn(checkIn);
        }else {
            checkIn = null;
        }
        return checkIn;
    }


    @Override
    public int approveCheckIn(Integer userId, Integer status) {
        Integer flag = 0;
        flag = checkInMapper.updateCheckIn(userId, status);
        houseApplyMapper.updateApply(userId);
        return flag;
    }
}
