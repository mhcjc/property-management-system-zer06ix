package com.zer06ix.pms.service;

import com.zer06ix.pms.entity.GoodsInspection;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author 浩
 */

public interface GoodsInsService {

    Integer addGoodInspection(GoodsInspection goodsInspection);

    Integer updateGoodsInsStatus(Integer inspectionId);

    BigDecimal account(Integer checkOutId);

    Integer updateRoomGoodsInsStatus(Integer checkOutId);


}
