package com.zer06ix.pms.service;

import com.zer06ix.pms.entity.Wepayment;
import io.swagger.models.auth.In;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface WepayService {
    List<Wepayment> expensesInfo(Integer roomId, Integer status);

    Integer paySuccess(Integer roomId, String payWay);

    List<Wepayment> expensesInfoLimit(Integer start,Integer end);

    Integer expensesInfoTotal();
}
