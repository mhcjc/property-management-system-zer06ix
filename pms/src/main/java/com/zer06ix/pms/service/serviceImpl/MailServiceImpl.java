package com.zer06ix.pms.service.serviceImpl;

import com.zer06ix.pms.service.MailService;
import com.zer06ix.pms.util.CheckCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.mail.Session;
import javax.servlet.http.HttpSession;
import java.net.URI;

@Service
public class MailServiceImpl implements MailService {

    @Autowired
    private JavaMailSender javaMailSender;


    @Override
    public String sendMail(String useremail ) {
        String flag = "0";
        String subject = new String("验证码");
        StringBuilder checkCode = CheckCode.Code();
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("1161530087@qq.com");
        message.setTo(useremail);
        message.setSubject(subject);
        message.setText("验证码(三分钟内有效):"+String.valueOf(checkCode));
        try {
            javaMailSender.send(message);
            flag = checkCode.toString();

        } catch (Exception e) {
            System.out.println("e = " + e);
        }
        return flag;
    }
}
