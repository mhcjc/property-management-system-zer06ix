/*
package com.zer06ix.pms.interceptor;

import com.auth0.jwt.interfaces.Claim;
import com.zer06ix.pms.util.JwtUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.Objects;

*/
/**
 * @author 聪
 *//*

public class TokenInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        //token验证
        if (!Objects.isNull(token)) {
            try {
                JwtUtils.verfyToken(token);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
*/
