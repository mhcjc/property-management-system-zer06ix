package com.zer06ix.pms.enums;

/**
 * @author 聪
 */

public enum CheckInStatus {
    RESIDE_STATUS_UNUSED(0, "未入住"),
    RESIDE_STATUS_USED(1, "已入住"),
    RESIDE_STATUS_CANCEL(2, "已取消")
    ;
    private Integer code;

    private String message;

    CheckInStatus(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }
    public String getMessage() {
        return message;
    }
}
