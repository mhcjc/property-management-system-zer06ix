package com.zer06ix.pms.enums;

/**
 * @author 聪
 */

public enum UserTypeEnum {
    //系统管理员admin
    SYSTEM_ADMIN(0),

    //物业基础档案管理员
    SYSTEM_PBFM(1),

    //员工宿舍管理
    SYSTEM_SDM(2),

    //档案管理员
    SYSTEM_FC(4),

    //普通用户
    SYSTEM_USER(8);


    private int typeCode;

    UserTypeEnum(int typeCode) {
        this.typeCode = typeCode;
    }

    public int getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(int typeCode) {
        this.typeCode = typeCode;
    }
}
