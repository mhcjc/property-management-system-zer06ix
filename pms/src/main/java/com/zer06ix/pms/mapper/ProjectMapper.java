package com.zer06ix.pms.mapper;

import com.zer06ix.pms.entity.Project;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 聪
 */
@Mapper
public interface ProjectMapper {
    List<Project> queryAllProjects();
}
