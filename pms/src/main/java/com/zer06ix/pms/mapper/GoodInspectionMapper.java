package com.zer06ix.pms.mapper;

import com.zer06ix.pms.entity.CheckOut;
import com.zer06ix.pms.entity.GoodsInspection;
import com.zer06ix.pms.entity.Room;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author 聪
 */
@Mapper
public interface GoodInspectionMapper {

    Integer addGoodInspection(GoodsInspection goodsInspection) ;

    Room queryRoomByCheckoutId(@Param("checkOutId") Integer checkOutId);

    List<GoodsInspection> queryAllGoodsInspection();

    GoodsInspection queryGoodsInsByinspectionId(@Param("inspectionId") Integer inspectionId);

    Integer updateGoodsInsStatus(@Param("inspectionId") Integer inspectionId);

    Integer updateRoomGoodsInsStatus(@Param("checkOutId") Integer checkOutId);

    List<GoodsInspection> queryGoodsInsBycheckOutId(@Param("checkOutId") Integer checkOutId);

    CheckOut queryCheckout(@Param("checkOutId") Integer checkOutId);

    Integer feedback(@Param("inspectionId") Integer inspectionId, @Param("remark") String remark);
}
