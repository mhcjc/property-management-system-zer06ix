package com.zer06ix.pms.mapper;

import com.zer06ix.pms.entity.CheckIn;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 聪
 */
@Mapper
public interface CheckInMapper {

    Integer addCheckIn(CheckIn checkIn);

    Integer cancelCheckIn(CheckIn checkIn);

    List<CheckIn> queryAllCheckIn();

    List<CheckIn> queryCheckInByUserId(Integer userId);

    CheckIn queryCheckInByCheckInId(@Param("userId") Integer CheckInId);

    Integer updateCheckIn(@Param("userId") Integer userId, @Param("status") Integer status);

}
