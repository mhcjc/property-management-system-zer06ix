package com.zer06ix.pms.mapper;

import com.zer06ix.pms.entity.Wepayment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 龙少2112
 */
@Mapper
public interface WepayMapper {
    List<Wepayment> queryWepaymentByRoomId(@Param("roomId") Integer roomId,@Param("status") Integer status);

    Integer updateWepaymentByRoomId(@Param("roomId") Integer roomId ,@Param("payWay") String payWay);

    List<Wepayment> queryWepaymentLimit(@Param("start") Integer start,@Param("end") Integer end);

    Integer queryWepaymentTotal();
}
