package com.zer06ix.pms.mapper;

import com.zer06ix.pms.entity.Suite;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 聪
 */
@Mapper
public interface SuiteMapper {
    List<Suite> querySuiteByBuildId(@Param("buildId") Integer buildId);

    List<Suite> querySuite();
}
