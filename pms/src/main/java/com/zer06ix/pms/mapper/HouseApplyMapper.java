package com.zer06ix.pms.mapper;

import com.zer06ix.pms.entity.HouseApply;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 聪
 */
@Mapper
public interface HouseApplyMapper {
    Integer addHouseApply(HouseApply houseApply);

    List<HouseApply> queryHouseApplyByUserId(@Param("userId") Integer userId);

    List<HouseApply> queryAllHouseApply();

    Integer updateHouseApply(HouseApply houseApply);

    Integer updateApply(Integer userId);

}
