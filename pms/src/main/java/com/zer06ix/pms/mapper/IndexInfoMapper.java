package com.zer06ix.pms.mapper;

import com.zer06ix.pms.entity.IndexInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 龙少2112
 */
@Mapper
public interface IndexInfoMapper {
    /**
     * 查询主页信息
     * @return index
     */
    List<IndexInfo> queryIndex();

    /**
     * 添加主页信息
     * @param indexInfo
     * @return 成功插入行数
     */
    Integer insertIndex(IndexInfo indexInfo);

    /**
     * 更改主页信息
     * @param indexInfo
     * @return 成功更改行数
     */
    Integer updateIndexByTextId(IndexInfo indexInfo);

    /**
     * 删除主页信息
     * @param textId
     * @return 成功删除行数
     */
    Integer deleteIndexByTextId(@Param("textId") Integer textId);

    /**
     * 用于查询主页信息总数
     * @return 记录总数
     */
    Integer queryIndexTotal();

    /**
     * 用于分页查询
     * @param start
     * @param end
     * @return Index实例
     */
    List<IndexInfo> queryIndexLimit(@Param("start") Integer start,@Param("end") Integer end);
}
