package com.zer06ix.pms.mapper;

import com.zer06ix.pms.entity.Bed;
import com.zer06ix.pms.entity.Build;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 聪
 */
@Mapper
public interface BuildMapper {
    List<Build> queryBuildByProjectId(@Param("projectId") Integer projectId);

    List<Build> queryAllBuild();

}
