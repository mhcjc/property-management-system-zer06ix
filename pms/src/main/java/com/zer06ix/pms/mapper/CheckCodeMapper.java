package com.zer06ix.pms.mapper;

import com.zer06ix.pms.entity.CheckCode;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface CheckCodeMapper {
    Integer insertCheckCode(@Param("email") String email,@Param("checkCode") String checkCode);
    CheckCode selectCheckCode(String email);
    Integer deleteCheckCode(String email);
}
