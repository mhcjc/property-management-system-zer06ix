package com.zer06ix.pms.mapper;

import com.zer06ix.pms.entity.Room;
import com.zer06ix.pms.entity.RoomRepair;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * @author 聪
 */
@Mapper
public interface RoomMapper {
    List<Room> queryRoomBySuiteId(@Param("suiteId") Integer suiteId);

    List<Room> queryAllRoom();

    Room queryRoomById(@Param("roomId")Integer roomId);

    Integer updateRoom(Room room);

    Integer insertRoomRepair(RoomRepair roomRepair);

    List<RoomRepair> queryRoomRepairById(@Param("roomId")Integer roomId);

    List<Room> queryRoomExist();

    Integer queryRoomRepairTotal();

    List<RoomRepair> queryRoomRepairLimit(@Param("start")Integer start,@Param("end") Integer end);

}
