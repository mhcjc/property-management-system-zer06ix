package com.zer06ix.pms.mapper;

import com.zer06ix.pms.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author 聪
 */
@Mapper
public interface UserMapper {
    User queryUserById(@Param("userId") Integer userId);

    User queryUserByIdAndPassword(@Param("userId") Integer userId, @Param("passwrd") Integer password);

    User queryUserByName(@Param("username") String username);

    Integer addUser(User user);

    User queryUserByPhoneNumber(String phoneNumber);

    User queryUserByEmailAndId(@Param("userId") Integer userId, @Param("email") String email);

    User queryUserByEmail(@Param("email") String email);

    Integer updateUserById(@Param("userId") Integer userId, @Param("newPassword") String newPassword);

    Integer updateUserStatus(@Param("userId") Integer userId, @Param("status") Integer status);

    Integer updateUserRoomId(@Param("bedId") Integer bedId, @Param("userId") Integer userId);

}
