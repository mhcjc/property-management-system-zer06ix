package com.zer06ix.pms.controller;

import com.zer06ix.pms.entity.CheckCode;
import com.zer06ix.pms.enums.ResultCode;
import com.zer06ix.pms.mapper.CheckCodeMapper;
import com.zer06ix.pms.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
@CrossOrigin
@Api(tags = "修改密码")
@RestController
@RequestMapping("/update")
@Transactional(rollbackFor = RuntimeException.class)
public class UpdatePasswordController {

    @Autowired
    @Qualifier("userServiceImpl")
    UserService userService;

    @Autowired
    CheckCodeMapper checkCodeMapper;

    @ApiOperation("忘记密码")
    @GetMapping("/forgetPassword")
    public Map<String, Object> forgetPassword(@RequestParam("userId") String userId,
                                              @RequestParam("newPassword") String newPassword,
                                              @RequestParam("checkCode") String checkCode,
                                              @RequestParam("useremail") String useremail) {
        Map<String, Object> map = new HashMap<>();
        CheckCode userCheckCode = checkCodeMapper.selectCheckCode(useremail);
        if(userCheckCode.getCheckCode() != null ){
            if (userCheckCode.getCheckCode().equals(checkCode)) {
                Integer flag = userService.forgetPassword(Integer.parseInt(userId),newPassword);
                if (flag != 0){
                    map.put("data", flag);
                    map.put("status", ResultCode.SUCCESS.getCode());
                    map.put("msg", ResultCode.SUCCESS.getMessage());
                    checkCodeMapper.deleteCheckCode(useremail);
                }else{
                    map.put("data", flag);
                    map.put("status", ResultCode.COMMON_FAIL.getCode());
                    map.put("msg", ResultCode.COMMON_FAIL.getMessage());
                }
            } else {
                map.put("data", 2);
                map.put("status", ResultCode.USER_CHECKCODE_ERROR.getCode());
                map.put("msg", ResultCode.USER_CHECKCODE_ERROR.getMessage());
            }
        }else{
            map.put("data", 2);
            map.put("status", ResultCode.USER_CHECKCODE_ERROR.getCode());
            map.put("msg", ResultCode.USER_CHECKCODE_ERROR.getMessage());
        }

        return map;
    }

    @ApiOperation("修改密码")
    @GetMapping("/changePassword")
    public Map<String, Object> changePassword(@RequestParam("userId") String userId,
                                              @RequestParam("newPassword") String newPassword,
                                              @RequestParam("oldPassword") String oldPassword,
                                              @RequestParam("checkCode") String checkCode,
                                              @RequestParam("useremail") String useremail) {
        Map<String, Object> map = new HashMap<>();
        CheckCode userCheckCode = checkCodeMapper.selectCheckCode(useremail);
        if(userCheckCode.getCheckCode() != null ){
            if (checkCode.equals(userCheckCode.getCheckCode())) {
                Integer flag = userService.changePassword(Integer.parseInt(userId),oldPassword,newPassword);
                if (flag != 0){
                    map.put("data", flag);
                    map.put("status", ResultCode.SUCCESS.getCode());
                    map.put("msg", ResultCode.SUCCESS.getMessage());
                    checkCodeMapper.deleteCheckCode(useremail);
                }else{
                    map.put("data", flag);
                    map.put("status", ResultCode.COMMON_FAIL.getCode());
                    map.put("msg", ResultCode.COMMON_FAIL.getMessage());
                }
            } else {
                map.put("data", 2);
                map.put("status", ResultCode.USER_CHECKCODE_ERROR.getCode());
                map.put("msg", ResultCode.USER_CHECKCODE_ERROR.getMessage());
            }
        }else{
            map.put("data", 2);
            map.put("status", ResultCode.USER_CHECKCODE_ERROR.getCode());
            map.put("msg", ResultCode.USER_CHECKCODE_ERROR.getMessage());
        }
        return map;
    }


}
