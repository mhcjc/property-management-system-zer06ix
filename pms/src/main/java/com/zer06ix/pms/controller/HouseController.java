package com.zer06ix.pms.controller;

import com.zer06ix.pms.entity.*;
import com.zer06ix.pms.enums.ResultCode;
import com.zer06ix.pms.enums.RoleStatusEnum;
import com.zer06ix.pms.enums.UserTypeEnum;
import com.zer06ix.pms.service.HouseService;
import com.zer06ix.pms.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author 聪
 */

@CrossOrigin
@Api(tags = "住房模块")
@RestController
@RequestMapping("/house")
public class HouseController {

    @Autowired
    @Qualifier("houseServiceImpl")
    private HouseService houseService;

    @Autowired
    @Qualifier("userServiceImpl")
    private UserService userService;

    @ApiOperation("住房查询")
    @GetMapping("/houseQuery")
    public Map<String, Object> houseQuery(@RequestParam(value = "projectId", required = false) String projectId,
                                          @RequestParam(value = "buildId", required = false) String buildId,
                                          @RequestParam(value = "suiteId", required = false) String suiteId,
                                          @RequestParam(value = "roomId", required = false) String roomId,
                                          @RequestParam(value = "roomStatus") String roomStatus,
                                          @RequestParam( value = "suiteType")String suiteType) {
        List<Room> rooms = new ArrayList<>();
        //判断projectId
        if (!"".equals(projectId) && projectId != null) {
            if (!"".equals(buildId) && buildId != null) {
                if (!"".equals(suiteId) && suiteId != null) {
                    if (!"".equals(roomId) && roomId != null) {
                        rooms.add(houseService.queryRoomByRoomId(Integer.valueOf(roomId)));
                    } else {
                        rooms = houseService.queryRoomBySuiteId(Integer.valueOf(suiteId));
                    }
                } else {
                    rooms = houseService.queryRoomByBuildId(Integer.valueOf(buildId));
                }
            } else {
                rooms = houseService.queryRoomByProjectId(Integer.valueOf(projectId));
            }
        } else {
            rooms = houseService.queryAllRoom();
        }
        List<Room> results = new ArrayList<>();
        for (Room room : rooms) {
            if (room.getRoomStatus() == Integer.parseInt(roomStatus)) {
                results.add(room);
            }
        }

        Map<String, Object> map = new HashMap<>();
        map.put("data", results);
        map.put("status", ResultCode.SUCCESS.getCode());
        map.put("msg", ResultCode.SUCCESS.getMessage());
        return map;
    }

    @ApiOperation("住房申请")
    @PostMapping("/houseApply")
    public Map<String, Object> houseApply(@RequestBody HouseApply houseApply) {
        HashMap<String, Object> map = new HashMap<>();
        int result = houseService.addHouseApply(houseApply);
        userService.changeStatus(houseApply.getUserId(), 2);
        map.put("data", result);
        if (result != 0) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("住房申请查询")
    @PostMapping("/queryHouseApply")
    public Map<String, Object> queryHouseApply(@RequestBody User user) {
        HashMap<String, Object> map = new HashMap<>();
        List<HouseApply> result = null;
        List<HouseApply> realResult = new ArrayList<>();
        if (user.getRoleId() == UserTypeEnum.SYSTEM_ADMIN.getTypeCode() || user.getRoleId() == UserTypeEnum.SYSTEM_SDM.getTypeCode()) {
            result = houseService.queryAllHouseApply();
        } else {
            result = houseService.queryHouseApplyByUserId(user.getUserId());
        }
        map.put("data", result);
        if (result != null) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("住房申请查询")
    @PostMapping("/findHouseApply")
    public Map<String, Object> findHouseApply(@RequestBody User user) {
        HashMap<String, Object> map = new HashMap<>();
        List<HouseApply> result = null;
        List<HouseApply> realResult = new ArrayList<>();
        if (user.getRoleId() == UserTypeEnum.SYSTEM_ADMIN.getTypeCode() || user.getRoleId() == UserTypeEnum.SYSTEM_SDM.getTypeCode()) {
            result = houseService.queryAllHouseApply();
        } else {
            result = houseService.queryHouseApplyByUserId(user.getUserId());
        }
        for (HouseApply houseApply : result) {
            if (houseApply.getApplyStatus() == 3){
                realResult.add(houseApply);
            }
        }
        map.put("data", realResult);
        if (realResult != null) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }


    @ApiOperation("同意住房申请")
    @PostMapping("/houseApplyApprove")
    public Map<String, Object> houseApplyApprove(@RequestBody HouseApply houseApply) {
        HashMap<String, Object> map = new HashMap<>();
        HouseApply result = houseService.approveHouseApply(houseApply);
        map.put("data", result);
        if (result != null) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("取消住房申请")
    @PostMapping("/cancelHouseApply")
    public Map<String, Object> cancelHouseApply(@RequestBody HouseApply houseApply) {
        HashMap<String, Object> map = new HashMap<>();
        HouseApply result = houseService.cancelHouseApply(houseApply);
        map.put("data", result);
        if (result != null) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("查询project")
    @GetMapping("/queryProject")
    public Map<String, Object> queryProject() {
        HashMap<String, Object> map = new HashMap<>();
        List<Project> result = houseService.queryAllProject();
        map.put("data", result);
        if (result != null) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("查询build")
    @GetMapping("/queryBuild")
    public Map<String, Object> queryBuild(@RequestParam("projectId")String projectId) {
        HashMap<String, Object> map = new HashMap<>();
        List<Build> result = houseService.queryBuildByProjectId(Integer.parseInt(projectId));
        map.put("data", result);
        if (result != null) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("查询suite")
    @GetMapping("/querySuite")
    public Map<String, Object> querySuite(@RequestParam("buildId")String buildId) {
        HashMap<String, Object> map = new HashMap<>();
        List<Suite> result = houseService.querySuiteByBuildId(Integer.parseInt(buildId));
        map.put("data", result);
        if (result != null) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("查询room")
    @GetMapping("/queryRoom")
    public Map<String, Object> queryRoom(@RequestParam("suiteId")String suiteId) {
        HashMap<String, Object> map = new HashMap<>();
        List<Room> result = houseService.queryRoomsBySuiteId(Integer.parseInt(suiteId));
        map.put("data", result);
        if (result != null) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }
}
