package com.zer06ix.pms.controller;

import com.zer06ix.pms.entity.GoodsInspection;
import com.zer06ix.pms.entity.Room;
import com.zer06ix.pms.enums.ResultCode;
import com.zer06ix.pms.mapper.GoodInspectionMapper;
import com.zer06ix.pms.service.GoodsInsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 浩
 */

@RestController
@Api(tags = "物品查验模块")
@RequestMapping("/GoodInspection")
public class GoodsInspectionController {

    @Autowired
    private GoodsInsService goodsInsService;

    @Autowired
    private GoodInspectionMapper goodInspectionMapper;

    @ApiOperation("加入物品查验表")
    @PostMapping("/add")
    public Map<String, Object> addGoodInspection(@RequestBody GoodsInspection goodsInspections){
        Map<String, Object> map = new HashMap<>();
        Integer result = goodsInsService.addGoodInspection(goodsInspections);
        if (result != 0){
            map.put("data",goodsInspections);
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("单个物品查验表状态修改")
    @PostMapping("/cancelins")
    public Map<String, Object> updateGoodsInsStatus(@RequestBody GoodsInspection inspection){
        Map<String,Object> map = new HashMap<>();
        Integer flag = goodsInsService.updateGoodsInsStatus(inspection.getInspectionId());
        map.put("data",flag);
        if (flag != 0) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("一个房间的物品查验表状态修改")
    @GetMapping("/cancelroom")
    public Map<String, Object> updateRoomGoodsInsStatus(@RequestParam("checkOutId")String checkOutId){
        Map<String,Object> map = new HashMap<>();
        Integer flag = goodsInsService.updateRoomGoodsInsStatus(Integer.parseInt(checkOutId));
        map.put("data",flag);
        if (flag != 0) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("查询物品查验表所在的房间信息")
    @GetMapping("/queryroom")
    public Map<String, Object> queryRoomByCheckoutId(@RequestBody String checkOutId){
        Map<String,Object> map = new HashMap<>();
        List<GoodsInspection> goodsInspections = goodInspectionMapper.queryGoodsInsBycheckOutId(Integer.parseInt(checkOutId));
        Room room = goodInspectionMapper.queryRoomByCheckoutId(Integer.parseInt(checkOutId));
        if (goodsInspections.size() != 0 && room != null){
            map.put("data",room);
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("查询一个房间的所有查验表")
    @PostMapping("/querygoodsIns")
    public Map<String, Object> queryGoodsInsBycheckOutId(@RequestBody String checkOutId){
        Map<String,Object> map = new HashMap<>();
        List<GoodsInspection> tempGoodsInspectionList = goodInspectionMapper.queryGoodsInsBycheckOutId(Integer.parseInt(checkOutId));
        List<GoodsInspection> goodsInspections = new ArrayList<>();
        for (GoodsInspection inspection : tempGoodsInspectionList) {
            if (inspection.getGoodsStatus() == 1){
                goodsInspections.add(inspection);
            }
        }
        if (goodsInspections.size() != 0){
            map.put("data",goodsInspections);
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("查询所有查验表")
    @PostMapping("/queryallgoodsIns")
    public Map<String, Object> queryAllGoodsInsBycheckOutId(){
        Map<String,Object> map = new HashMap<>();
        List<GoodsInspection> goodsInspections = goodInspectionMapper.queryAllGoodsInspection();
        if (goodsInspections.size() != 0){
            map.put("data",goodsInspections);
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("结算中心")
    @PostMapping("/account")
    public Map<String, Object> account(@RequestBody String checkOutId){
        Map<String,Object> map = new HashMap<>();
        BigDecimal totalprice = goodsInsService.account(Integer.parseInt(checkOutId));
        if (totalprice.compareTo(BigDecimal.ZERO) != 0) {
            map.put("data",totalprice);
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("用户反馈")
    @GetMapping("/feedback")
    public Map<String, Object> feedback(@RequestParam("inspectionId")String inspectionId,@RequestParam("remark")String remark){
        Map<String,Object> map = new HashMap<>();
        Integer flag = goodInspectionMapper.feedback(Integer.parseInt(inspectionId),remark);
        map.put("data",flag);
        if (flag != 0) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

}
