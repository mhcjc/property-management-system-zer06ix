package com.zer06ix.pms.controller;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zer06ix.pms.entity.CheckOut;
import com.zer06ix.pms.enums.ResultCode;
import com.zer06ix.pms.service.CheckOutService;
import com.zer06ix.pms.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 龙少2112
 */

@CrossOrigin
@RestController
@Api(tags = "退场模块")
@RequestMapping("/checkOut")
public class CheckOutController {

    @Autowired
    @Qualifier("checkOutServiceImpl")
    CheckOutService checkOutService;

    @Autowired
    private UserService userService;

    @ApiOperation("申请退场")
    @PostMapping("/applyCheckout")
    public Map<String, Object> applyCheckOut(@RequestBody CheckOut checkOut) {
        Map<String, Object> map = new HashMap<>();
        Integer flag = checkOutService.insertCheckOut(checkOut);
        map.put("data", flag);
        if (flag == 1) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("修改退场日期")
    @PostMapping("/changeDateByUserId")
    public Map<String, Object> changeDate(@RequestBody CheckOut checkOut) {
        Map<String, Object> map = new HashMap<>();
        Integer flag = checkOutService.updateCheckOutByUserId(checkOut);
        map.put("data", flag);
        if (flag == 1) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("退场个人信息查询")
    @GetMapping("/queryPersonalCheckOut")
    public Map<String, Object> queryPersonalCheckOut(@RequestParam("userId") String userId) {
        Map<String, Object> map = new HashMap<>();
        CheckOut checkOut = checkOutService.queryCheckOutByUserId(Integer.parseInt(userId));
        if (checkOut != null) {
            map.put("data", checkOut);
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("data", 0);
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }


    @ApiOperation("退场状态修改")
    @GetMapping("/changeStatus")
    public Map<String, Object> changeStatus(@RequestParam("userId") String userId,@RequestParam("checkOutStatus") String checkOutStatus) {
        Map<String, Object> map = new HashMap<>();
        Integer flag = checkOutService.updateCheckOutStatus(Integer.parseInt(userId),Integer.parseInt(checkOutStatus));
        map.put("data", flag);
        if (flag == 1) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("退场所有信息查询")
    @GetMapping("/queryAllCheckOut")
    public Map<String, Object> queryAllCheckOut() {
        Map<String, Object> map = new HashMap<>();
        List<CheckOut> checkOuts = checkOutService.checkOutAll();
        List<CheckOut> result = new ArrayList<>();
        for (CheckOut checkOut : checkOuts) {
            if (checkOut.getLeaveStatus() != 3){
                result.add(checkOut);
            }
        }
        Integer total = checkOutService.cheeckOutTotal();
        map.put("total",total);
        map.put("data", result);
        map.put("status", ResultCode.SUCCESS.getCode());
        map.put("msg", ResultCode.SUCCESS.getMessage());
        return map;
    }
    @ApiOperation("通过roomId查询checkOutId")
    @PostMapping("/queryCheckOutIdByRoomId")
    public Map<String, Object> queryCheckOutIdByRoomId(@RequestBody Integer roomId) {
        Map<String, Object> map = new HashMap<>();
        Integer result = checkOutService.queryCheckOutIdByRoomId(roomId);
        if (result != null) {
            map.put("data", result);
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("同意退场")
    @GetMapping("/approveCheckOut")
    public Map<String, Object> approveCheckOut(@RequestParam("userId") String userId,@RequestParam("status") String status) {
        Map<String, Object> map = new HashMap<>();
        int flag = checkOutService.updateCheckOutStatus(Integer.parseInt(userId),Integer.parseInt(status));
        userService.changeStatus(Integer.parseInt(userId),1);
        map.put("data", flag);
        if (flag != 0) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }
}
