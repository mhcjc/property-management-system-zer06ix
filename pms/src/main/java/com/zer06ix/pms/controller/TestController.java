package com.zer06ix.pms.controller;

import com.zer06ix.pms.entity.User;
import com.zer06ix.pms.enums.ResultCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 聪
 */
@CrossOrigin
@Api(tags = "测试")
@RestController
@RequestMapping("/test")
public class TestController {

    @ApiOperation("测试集合")
    @PostMapping("/map")
    @ResponseBody
    public Map<String, Object> isUsernameExist(@RequestBody User user) {
        System.out.println("user = " + user);

        Map<String, Object> map = new HashMap<>();
        if (user == null) {
            map.put("user", null);
            map.put("status", ResultCode.USER_PHONENUMBER_ALREADY_EXIST.getCode());
            map.put("msg", ResultCode.USER_PHONENUMBER_ALREADY_EXIST.getMessage());
        } else {
            map.put("user", user);
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        }
        return map;
    }
}
