package com.zer06ix.pms.controller;

import com.zer06ix.pms.entity.Wepayment;
import com.zer06ix.pms.enums.ResultCode;
import com.zer06ix.pms.service.WepayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.jdbc.Null;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 龙少2112
 */
@CrossOrigin
@Api(tags = "水电缴费模块")
@RestController
@RequestMapping("/wepay")
@Transactional(rollbackFor = RuntimeException.class)
public class WepayController {

    @Autowired
    @Qualifier("wepayServiceImpl")
    WepayService wepayService;

    @ApiOperation("个人缴费信息")
    @GetMapping("/personalExpensesInfo")
    public Map<String, Object> personalExpensesInfo(@RequestParam("roomId") String roomId,@RequestParam("status") String status) {
        Map<String, Object> map = new HashMap<>();
        List<Wepayment> wepayment = wepayService.expensesInfo(Integer.parseInt(roomId),Integer.parseInt(status));
        if(!wepayment.isEmpty()){
            map.put("data", wepayment);
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("data", null);
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("分页缴费信息")
    @GetMapping("/expensesInfo")
    public Map<String, Object> expensesInfo(@RequestParam("page") String page,@RequestParam("row") String row) {
        Map<String, Object> map = new HashMap<>();
        Integer start = Integer.valueOf(page);
        Integer end = Integer.valueOf(row) ;
        Integer begin = (start - 1 ) * end ;
        List<Wepayment> wepayment = wepayService.expensesInfoLimit(begin,end);
        Integer total = wepayService.expensesInfoTotal();
        map.put("data", wepayment);
        map.put("total", total);
        map.put("status", ResultCode.SUCCESS.getCode());
        map.put("msg", ResultCode.SUCCESS.getMessage());
        return map;
    }

    @ApiOperation("缴费成功")
    @GetMapping("/paySuccess")
    public Map<String, Object> paySuccess(@RequestParam("roomId") String roomId, @RequestParam("payWay") String payWay) {
        Map<String, Object> map = new HashMap<>();
        Integer flag = wepayService.paySuccess(Integer.parseInt(roomId), payWay);
        if (flag == 1) {
            map.put("data", flag);
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("data", flag);
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }
}
