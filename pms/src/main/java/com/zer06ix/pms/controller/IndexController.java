package com.zer06ix.pms.controller;

import com.zer06ix.pms.entity.IndexInfo;
import com.zer06ix.pms.enums.ResultCode;
import com.zer06ix.pms.service.IndexService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 龙少2112
 */
@Api(tags = "主页")
@RestController
@CrossOrigin
@RequestMapping("/index")
public class IndexController {
    @Autowired
    @Qualifier("indexServiceImpl")
    IndexService indexService;

    @ApiOperation("填入主页信息")
    @GetMapping("/insertIndex")
    public Map<String, Object> insertIndex(@RequestBody IndexInfo indexInfo) {
        System.out.println("index = " + indexInfo);
        Map<String, Object> map = new HashMap<>();
        Integer flag = indexService.insertIndex(indexInfo);
        map.put("data", indexInfo);
        if (flag == 1) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("查询主页信息")
    @PostMapping("/queryIndex")
    public Map<String, Object> queryIndex() {
        Map<String, Object> map = new HashMap<>();
        List<IndexInfo> indexInfos = indexService.queryIndex();
        if (indexInfos != null) {
            map.put("data", indexInfos);
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("data", 0);
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("更新主页信息")
    @GetMapping("/updateIndex")
    public Map<String, Object> updateIndex(@RequestBody IndexInfo indexInfo) {
        Map<String, Object> map = new HashMap<>();
        Integer flag = indexService.updateIndex(indexInfo);
        map.put("data", flag);
        if (flag == 1) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("删除主页信息")
    @GetMapping("/deleteIndex")
    public Map<String, Object> deleteIndex(@RequestParam("textId") String textId) {
        Map<String, Object> map = new HashMap<>();
        Integer flag = indexService.deleteIndex(Integer.parseInt(textId));
        map.put("data", flag);
        if (flag == 1) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("分页查询主页信息")
    @GetMapping("/queryIndexLimit")
    public Map<String, Object> queryIndexLimit(@RequestParam("page") String page,@RequestParam("row") String row) {
        Map<String, Object> map = new HashMap<>();
        Integer start = Integer.valueOf(page);
        Integer end = Integer.valueOf(row) ;
        Integer begin = (start - 1 ) * end ;
        List<IndexInfo> indexInfos = indexService.queryIndexLimit(begin,end);
        Integer total = indexService.queryIndexTotal();
        map.put("total",total);
        map.put("data", indexInfos);
        map.put("status", ResultCode.SUCCESS.getCode());
        map.put("msg", ResultCode.SUCCESS.getMessage());
        return map;
    }

}
