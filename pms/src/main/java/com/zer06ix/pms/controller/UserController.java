package com.zer06ix.pms.controller;

import com.zer06ix.pms.entity.User;
import com.zer06ix.pms.enums.ResultCode;
import com.zer06ix.pms.mapper.CheckCodeMapper;
import com.zer06ix.pms.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 龙少2112
 */
@Api(tags = "用户模块")
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    @Qualifier("userServiceImpl")
    UserService userService;

    /**
     * 判断用户名是否存在
     *
     * @param userId
     * @return flag标志
     */
    @ApiOperation("判断用户名是否存在")
    @GetMapping("/isUsernameExist")
    public Map<String, Object> isUsernameExist(@RequestParam("userId") String userId) {
        Map<String, Object> map = new HashMap<>();
        Integer id = Integer.parseInt(userId);
        User user = userService.isUsernameExist(id);
        if (user != null) {
            map.put("data", user);
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("data", null);
            map.put("status", ResultCode.USER_ACCOUNT_NOT_EXIST.getCode());
            map.put("msg", ResultCode.USER_ACCOUNT_NOT_EXIST.getMessage());
        }
        return map;
    }

    /**
     * 登录验证
     *
     * @param userId
     * @param password
     * @return user对象
     */
    @ApiOperation("登陆验证")
    @GetMapping("/login")
    public Map<String, Object> login(@RequestParam("userId") String userId, @RequestParam("password") String password) {
        Map<String, Object> map = new HashMap<>();
        Integer id = Integer.parseInt(userId);
        User user = userService.login(id, password);
        if (user != null) {
            map.put("data", user);
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("data","");
            map.put("status", ResultCode.USER_CREDENTIALS_ERROR.getCode());
            map.put("msg", ResultCode.USER_CREDENTIALS_ERROR.getMessage());
        }
        return map;
    }

    @ApiOperation("注册")
    @PostMapping("/signUp")
    public Map<String, Object> signUp(@RequestBody User user) {
        Map<String, Object> map = new HashMap<>();
        User signUser = userService.signUp(user);
        if (signUser == null) {
            map.put("data", signUser);
            map.put("status", ResultCode.USER_PHONENUMBER_ALREADY_EXIST.getCode());
            map.put("msg", ResultCode.USER_PHONENUMBER_ALREADY_EXIST.getMessage());
        } else {
            map.put("data", signUser);
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        }
        return map;
    }

}
