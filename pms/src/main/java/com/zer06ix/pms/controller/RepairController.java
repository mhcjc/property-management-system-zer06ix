package com.zer06ix.pms.controller;

import com.zer06ix.pms.entity.IndexInfo;
import com.zer06ix.pms.entity.RoomRepair;
import com.zer06ix.pms.entity.Wepayment;
import com.zer06ix.pms.enums.ResultCode;
import com.zer06ix.pms.mapper.RoomMapper;
import com.zer06ix.pms.mapper.UserMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@Api(tags = "报修模块")
@RestController
@RequestMapping("/room")
public class RepairController {

    @Autowired
    RoomMapper roomMapper;

    @ApiOperation("申请报修")
    @PostMapping("/applyRepair")
    public Map<String, Object> applyRepair(@RequestBody RoomRepair roomRepair) {
        Map<String, Object> map = new HashMap<>();
        if(roomRepair.getRemark() == null){
            roomRepair.setRemark("");
        }
        Integer flag = roomMapper.insertRoomRepair(roomRepair);
        map.put("data",flag);
        if (flag == 1) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.USER_ACCOUNT_NOT_EXIST.getCode());
            map.put("msg", ResultCode.USER_ACCOUNT_NOT_EXIST.getMessage());
        }
        return map;
    }

    @ApiOperation("报修信息查询")
    @GetMapping("/repairInfo")
    public Map<String, Object> repairInfo(@RequestParam("roomId") String roomId) {
        Map<String, Object> map = new HashMap<>();
        List<RoomRepair> roomRepair = roomMapper.queryRoomRepairById(Integer.parseInt(roomId));
        map.put("data",roomRepair);
        map.put("status", ResultCode.SUCCESS.getCode());
        map.put("msg", ResultCode.SUCCESS.getMessage());
        return map;
    }

    @ApiOperation("分页报修信息查询")
    @GetMapping("/repairInfoLimit")
    public Map<String, Object> repairInfoLimit(@RequestParam("page") String page,@RequestParam("row") String row) {
        Map<String, Object> map = new HashMap<>();
        Integer start = Integer.valueOf(page);
        Integer end = Integer.valueOf(row) ;
        Integer begin = (start - 1 ) * end ;
        List<RoomRepair> indexInfos = roomMapper.queryRoomRepairLimit(begin, end);
        Integer total = roomMapper.queryRoomRepairTotal();
        map.put("total",total);
        map.put("data", indexInfos);
        map.put("status", ResultCode.SUCCESS.getCode());
        map.put("msg", ResultCode.SUCCESS.getMessage());
        return map;
    }

}
