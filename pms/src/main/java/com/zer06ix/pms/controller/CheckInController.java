package com.zer06ix.pms.controller;

import com.zer06ix.pms.entity.CheckIn;
import com.zer06ix.pms.entity.User;
import com.zer06ix.pms.enums.ResultCode;
import com.zer06ix.pms.service.CheckInService;
import com.zer06ix.pms.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 聪
 */
@RestController
@CrossOrigin
@RequestMapping("/checkIn")
@Api(tags = "进场模块")
public class CheckInController {

    @Resource
    private CheckInService checkInService;

    @Autowired
    @Qualifier("userServiceImpl")
    private UserService userService;

    @ApiOperation("根据User查询列表")
    @PostMapping("/query")
    public Map<String, Object> queryCheckIn( @RequestBody User user) {
        Map<String, Object> map = new HashMap<>();
        List<CheckIn> result = checkInService.queryCheckIn(user);
        map.put("data", result);
        map.put("status", ResultCode.SUCCESS.getCode());
        map.put("msg", ResultCode.SUCCESS.getMessage());
        return map;
    }

    @ApiOperation("添加表单CheckIn（入场）申请入场")
    @PostMapping("/add")
    public Map<String, Object> addCheckIn(@RequestBody CheckIn checkIn) {
        Map<String, Object> map = new HashMap<>();
        CheckIn result = checkInService.addCheckIn(checkIn);
        map.put("data", result);
        map.put("status", ResultCode.SUCCESS.getCode());
        map.put("msg", ResultCode.SUCCESS.getMessage());
        return map;
    }

    @ApiOperation("添加表单CheckIn（入场）取消入场")
    @GetMapping("/cancel")
    public Map<String, Object> cancelCheckIn(@RequestBody CheckIn checkIn) {
        Map<String, Object> map = new HashMap<>();
        CheckIn result = checkInService.cancelCheckIn(checkIn);
        map.put("data", result);
        if (result != null) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("添加表单CheckIn（入场）已入住")
    @GetMapping("/approve")
    public Map<String, Object> approveCheckIn(@RequestParam("userId") String userId,@RequestParam("status") String status) {
        Map<String, Object> map = new HashMap<>();
        int flag = checkInService.approveCheckIn(Integer.parseInt(userId),Integer.parseInt(status));
        userService.changeStatus(Integer.parseInt(userId),1);
        map.put("data", flag);
        if (flag != 0) {
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }
}
