package com.zer06ix.pms.controller;

import com.zer06ix.pms.entity.CheckCode;
import com.zer06ix.pms.entity.User;
import com.zer06ix.pms.enums.ResultCode;
import com.zer06ix.pms.mapper.CheckCodeMapper;
import com.zer06ix.pms.mapper.UserMapper;
import com.zer06ix.pms.service.MailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 龙少2112
 */
@Api(tags = "邮箱模块")
@CrossOrigin
@RestController
@RequestMapping("/emailTest")
public class EmailTestController {

    @Autowired
    @Qualifier("mailServiceImpl")
    MailService mailService;

    @Autowired
    UserMapper userMapper;

    @Autowired
    CheckCodeMapper checkCodeMapper;

    @ApiOperation("忘记密码或修改密码获取验证码")
    @GetMapping("/code")
    public Map<String, Object> code(@RequestParam("userId") String userId, @RequestParam("useremail") String useremail) {
        Map<String, Object> map = new HashMap<>();
        User user = userMapper.queryUserById(Integer.parseInt(userId));
        if(user != null){
            if(useremail.equals(user.getEmail())){
                String checkCode = mailService.sendMail(useremail);
                if (!"".equals(checkCode)) {
                    checkCodeMapper.insertCheckCode(useremail,checkCode);
                    map.put("data", 1);
                    map.put("status", ResultCode.SUCCESS.getCode());
                    map.put("msg", ResultCode.SUCCESS.getMessage());
                } else {
                    map.put("data", 0);
                    map.put("status", ResultCode.COMMON_FAIL.getCode());
                    map.put("msg", ResultCode.COMMON_FAIL.getMessage());
                }
            }else{
                map.put("data", 0);
                map.put("status", ResultCode.USER_EMAIL_ERROR.getCode());
                map.put("msg", ResultCode.USER_EMAIL_ERROR.getMessage());
            }
        }else{
            map.put("data", 0);
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }

        return map;
    }

    @ApiOperation("注册获取验证码")
    @GetMapping("/signUpCode")
    public Map<String, Object> signUpCode(@RequestParam("useremail") String useremail) {
        Map<String, Object> map = new HashMap<>();
        String checkCode = mailService.sendMail(useremail);
        if (!"".equals(checkCode)) {
            checkCodeMapper.insertCheckCode(useremail,checkCode);
            map.put("data", 1);
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
        } else {
            map.put("data", 0);
            map.put("status", ResultCode.COMMON_FAIL.getCode());
            map.put("msg", ResultCode.COMMON_FAIL.getMessage());
        }
        return map;
    }

    @ApiOperation("检验验证码")
    @GetMapping("/checkCode")
    public Map<String, Object> checkCode(@RequestParam("useremail") String useremail, @RequestParam("code") String code) {
        Map<String, Object> map = new HashMap<>();
        CheckCode checkCode = checkCodeMapper.selectCheckCode(useremail);
        if (code.equals(checkCode.getCheckCode())) {
            map.put("data", 1);
            map.put("status", ResultCode.SUCCESS.getCode());
            map.put("msg", ResultCode.SUCCESS.getMessage());
            checkCodeMapper.deleteCheckCode(useremail);
        } else {
            map.put("data", 0);
            map.put("status", ResultCode.USER_CHECKCODE_ERROR.getCode());
            map.put("msg", ResultCode.USER_CHECKCODE_ERROR.getMessage());
        }
        return map;
    }

    @ApiOperation("检验邮箱和userId")
    @GetMapping("/checkEmailAndUserId")
    public Map<String, Object> checkEmailAndUserId(@RequestParam("userId") String userId,@RequestParam("useremail") String useremail) {
        Map<String, Object> map = new HashMap<>();
        User user = userMapper.queryUserByEmailAndId(Integer.parseInt(userId),useremail);
        if (user != null) {
            map.put("data", 0);
            map.put("status", ResultCode.USER_EMAIL_ALREADY_EXIST.getCode());
            map.put("msg", ResultCode.USER_EMAIL_ALREADY_EXIST.getMessage());
        } else {
            map.put("data", 1);
            map.put("status", ResultCode.USER_EMAIL_NOT_EXIST.getCode());
            map.put("msg", ResultCode.USER_EMAIL_NOT_EXIST.getMessage());
        }
        return map;
    }

    @ApiOperation("检验邮箱")
    @GetMapping("/checkEmail")
    public Map<String, Object> checkEmail(@RequestParam("useremail") String useremail) {
        Map<String, Object> map = new HashMap<>();
        User user = userMapper.queryUserByEmail(useremail);
        if (user != null) {
            map.put("data", 0);
            map.put("status", ResultCode.USER_EMAIL_ALREADY_EXIST.getCode());
            map.put("msg", ResultCode.USER_EMAIL_ALREADY_EXIST.getMessage());
        } else {
            map.put("data", 1);
            map.put("status", ResultCode.USER_EMAIL_NOT_EXIST.getCode());
            map.put("msg", ResultCode.USER_EMAIL_NOT_EXIST.getMessage());
        }
        return map;
    }
}
